# this makefile fragment defines the external libraries
# for the libraries, examples, tests, and binaries
# 
# the following definitions must be output from this file:
# EXT_LIB_SEARCH_PATH : the search paths for libraries
# EXT_LINK_LIBS       : the libraries with which to link (in the proper link order)
# EXT_INC_SEARCH_PATH : the search paths for include files
EXT_LIB_SEARCH_PATH :=
EXT_LINK_LIBS       :=
EXT_INC_SEARCH_PATH :=

#LAPACK_BASE := /local/lapack
#EXT_INC_SEARCH_PATH += $(LAPACK_BASE)/include
#EXT_LIB_SEARCH_PATH += $(LAPACK_BASE)/lib
#EXT_LINK_LIBS  += -llapack

#BLAS_BASE:=/local/atlas
#EXT_INC_SEARCH_PATH += $(BLAS_BASE)/include
#EXT_LIB_SEARCH_PATH += $(BLAS_BASE)/lib
#EXT_LINK_LIBS  += -lblas

# this is an optional definition
# EXT_COMPILER_DEFINES : definitions passed to the compiler (without the -D option)
#EXT_CPP_OPTS := compiler options for c++ compilation
#EXT_C_OPTS := compiler options for c compilation
#EXT_FORTRAN_OPTS := compiler options for fortran compilation

EXT_COMPILER_DEFINES :=
EXT_CPP_OPTS := 
EXT_C_OPTS := 
EXT_FORTRAN_OPTS := 
