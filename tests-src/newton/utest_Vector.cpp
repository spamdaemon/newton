#include <newton/Vector.h>

typedef ::newton::Vector< double> Vector;

void utest_Constructors()
{
   Vector v;
   Vector u(4);
   Vector w(1.0, 2.0);
   Vector x(1.0, 2.0, 3.0);
   Vector y(1, 2, 3, 4);
   Vector z(x);
   double s[5];
   Vector vs(5, s);

   assert(u.dimension() == 4);
   assert(w.dimension() == 2);
   assert(x.dimension() == 3);
   assert(y.dimension() == 4);
   assert(z.dimension() == x.dimension());
   assert(vs.dimension() == 5);

   vs = u;
   vs = v;
}

void utest_AccessorsAndMutators()
{
   Vector u(1., 2, 3);
   assert(u(0) == 1.0);
   assert(u.get(0) == 1.0);
   u.set(1, -1);
   assert(u(1) == -1);

   u(0) += 1;
   assert(u(0) == 2);

}

void utest_Operators()
{
   Vector u(3., 4);
   Vector O(0.0, 0);
   assert(u.sqrLength() == 25);
   assert(u.length() == 5);
   assert(u.sqrDistance(O) == 25);

   u.negate();
   assert(u(0) == -3 && u(1) == -4);

   assert(u.mul(Vector(-1., -1)) == 7);
   u.scale(-1);
   assert(u(0) == 3 && u(1) == 4);

   u.invScale(4);
   assert(u(0) == 0.75 && u(1) == 1);

   u.add(Vector(2.25, 3));
   assert(u(0) == 3 && u(1) == 4);
   u.sub(Vector(6., 8));
   assert(u(0) == -3 && u(1) == -4);

   u.add(2, Vector(3., 4));
   assert(u(0) == 3 && u(1) == 4);
   u.sub(2, Vector(3., 4));
   assert(u(0) == -3 && u(1) == -4);
}

void utest_externalBuffer()
{
   double data[] = { 3, 4 };
   Vector u = Vector::createWithBuffer(2, data);
   Vector O(0.0, 0);
   assert(u.sqrLength() == 25);
   assert(u.length() == 5);
   assert(u.sqrDistance(O) == 25);

   u.negate();
   assert(u(0) == -3 && u(1) == -4);

   assert(u.mul(Vector(-1., -1)) == 7);
   u.scale(-1);
   assert(u(0) == 3 && u(1) == 4);

   u.invScale(4);
   assert(u(0) == 0.75 && u(1) == 1);

   u.add(Vector(2.25, 3));
   assert(u(0) == 3 && u(1) == 4);
   u.sub(Vector(6., 8));
   assert(u(0) == -3 && u(1) == -4);

   u.add(2, Vector(3., 4));
   assert(u(0) == 3 && u(1) == 4);
   u.sub(2, Vector(3., 4));
   assert(u(0) == -3 && u(1) == -4);

}

void utest_copy()
{
   {
      Vector m(2);
      m(1) = 10;
      Vector x(m);
      m(1) = 100;
      assert(x.dimension() == 2);
      assert(m.dimension() == 2);
      assert(m(1) == 100);
      assert(x(1) == 10);
   }
   {
      Vector m(2);
      Vector x(3);
      m(1) = 10;
      x = m;
      m(1) = 100;
      assert(x.dimension() == 2);
      assert(m.dimension() == 2);
      assert(m(1) == 100);
      assert(x(1) == 10);
   }
}
void utest_move()
{
   {
      Vector m(2);
      m(1) = 10;
      Vector x(::std::move(m));
      assert(x.dimension() == 2);
      assert(m.dimension() == 0);
      assert(x(1) == 10);
   }
   {
      Vector m(2);
      m(1) = 10;
      Vector x(3);
      x = ::std::move(m);
      assert(x.dimension() == 2);
      assert(m.dimension() == 0);
      assert(x(1) == 10);
   }
}
