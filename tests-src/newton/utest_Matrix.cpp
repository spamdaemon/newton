#include <newton/Matrix.h>
#include <iostream>

using namespace ::std;

typedef ::newton::Matrix< > Matrix;
typedef ::newton::Vector< > Vector;

static void checkMatrix(Matrix A, double a00, double a01, double a10, double a11)
{
   Matrix C(2, 2);
   C(0, 0) = a00;
   C(0, 1) = a01;
   C(1, 0) = a10;
   C(1, 1) = a11;

   assert(A == C);
}

void utest_Constructors()
{
   Matrix m;
   Matrix n(5);
   Matrix q(2, 3);
   Matrix O(0, 0);
   Matrix p(q);

   assert(n.rowSize() == 5 && n.colSize() == 5);
   assert(q.rowSize() == 2 && q.colSize() == 3);
   assert(p.rowSize() == 2 && p.colSize() == 3);
   assert(n.isSquare());

}

void utest_MatrixVectorOperations()
{
   Matrix m(2, 2);
   Vector v(1.0, 1.0);
   m(0, 0) = 2;
   m(0, 1) = 1;
   m(1, 0) = 8;
   m(1, 1) = -4;

   assert(m.postMultiply(v) == Vector(10, -3));
   assert(m.preMultiply(v) == Vector(3, 4));
}

void utest_MatrixMultiplication()
{
   Matrix m(2, 2);
   Matrix L(3, 2);
   Matrix R(2, 3);

   m(0, 0) = 1;
   m(0, 1) = 2;
   m(1, 0) = 3;
   m(1, 1) = 4;

   L(0, 0) = 1;
   L(0, 1) = 2;
   L(1, 0) = 3;
   L(1, 1) = 4;
   L(2, 0) = 5;
   L(2, 1) = 6;

   R(0, 0) = 1;
   R(1, 0) = 4;
   R(0, 1) = 2;
   R(1, 1) = 5;
   R(0, 2) = 3;
   R(1, 2) = 6;

   Matrix Lm = L * m;
   Matrix mR = m * R;

   Matrix checkLm(3, 2);
   checkLm(0, 0) = 7;
   checkLm(0, 1) = 10;
   checkLm(1, 0) = 15;
   checkLm(1, 1) = 22;
   checkLm(2, 0) = 23;
   checkLm(2, 1) = 34;

   Matrix checkmR(2, 3);
   checkmR(0, 0) = 9;
   checkmR(0, 1) = 12;
   checkmR(0, 2) = 15;
   checkmR(1, 0) = 19;
   checkmR(1, 1) = 26;
   checkmR(1, 2) = 33;

   assert(checkLm == Lm);
   assert(checkmR == mR);
}

void utest_MatrixOps()
{
   Matrix q(2, 2);
   q(0, 0) = 3;
   q(0, 1) = 4;
   q(1, 0) = 4;
   q(1, 1) = 3;

   Matrix p(2, 2);
   p(0, 0) = 1;
   p(0, 1) = 2;
   p(1, 0) = 3;
   p(1, 1) = 4;

   checkMatrix(q - p, 2, 2, 1, -1);
   checkMatrix(q + p, 4, 6, 7, 7);
   checkMatrix(q * 2, 6, 8, 8, 6);
   checkMatrix(2 * q, 6, 8, 8, 6);
   checkMatrix(q / 2, 1.5, 2, 2, 1.5);
   checkMatrix(q - 2 * p, 1, 0, -2, -5);
   checkMatrix(q + 2 * p, 5, 8, 10, 11);
}

void utest_MatrixInverse()
{
   Matrix q(1);
   q(0, 0) = 4;
   Matrix p = q.inverse();
   ::std::cerr << "p(0,0) = " << p(0, 0) << ::std::endl;
   assert(p(0, 0) == 0.25);

   double det = -1;

   q = Matrix(2);
   q(0, 0) = 4;
   q(1, 1) = 2;
   p = q.inverse((Matrix*) 0, &det);
   assert(abs(det - q.determinant()) < .00001);

   assert(p(0, 0) == 0.25);
   assert(p(0, 1) == 0);
   assert(p(1, 0) == 0);
   assert(p(1, 1) == 0.50);

   q = Matrix(2);
   q(1, 0) = 4;
   q(0, 1) = 2;
   p = q.inverse((Matrix*) 0, &det);
   assert(abs(det - q.determinant()) < .00001);
   ::std::cerr << "matrix I :" << ::std::endl << p * q << ::std::endl;
   assert(p(0, 0) == 0);
   assert(p(0, 1) == 0.25);
   assert(p(1, 0) == 0.50);
   assert(p(1, 1) == 0.0);

   q = Matrix(4);
   for (size_t i = 0; i < q.rowSize(); ++i) {
      for (size_t j = 0; j <= i; ++j) {
         q(i, j) = 3 * i + j;
         q(j, i) = 3 * i + j + 1;
      }
   }
   p = q.inverse((Matrix*) 0, &det);
   assert(abs(det - q.determinant()) < .00001);
   Matrix I = p / p;

   ::std::cerr << "matrix I :" << ::std::endl << I << ::std::endl;
   assert((I - Matrix::identity(p.rowSize())).abs().max() < 0.0001);
}

void utest_inverseMultiply()
{
   Matrix m(2);
   m(0, 0) = 1;
   m(0, 1) = 4;
   m(1, 0) = 4;
   m(1, 1) = 25;

   Matrix r(2, 3);
   r(0, 0) = 1;
   r(0, 1) = 2;
   r(0, 2) = 3;
   r(1, 0) = 3;
   r(1, 1) = 2;
   r(1, 2) = 1;

   Matrix A = m.inverse() * r;
   Matrix B = m.inverse(&r);
   ::std::cerr << "A: " << A << endl;
   ::std::cerr << "B: " << B << endl;
   assert((A - B).abs().max() < 0.001);
}

void utest_determinant()
{
   {
      Matrix m(3);
      m(2, 0) = 3;
      m(1, 2) = 3;
      m(0, 1) = 3;
      assert(m.determinant() == 27);
   }

   {
      Matrix m(3);
      m(2, 0) = 3;
      m(1, 1) = 3;
      m(0, 2) = 3;

      assert(m.determinant() == -27);
   }
}

void utest_multiplyTransposed()
{
   Matrix A(3);
   A(0, 0) = 1;
   A(0, 1) = 2;
   A(0, 2) = 3;
   A(1, 0) = 1;
   A(1, 1) = 4;
   A(1, 2) = 9;
   A(2, 0) = 1;
   A(2, 1) = 8;
   A(2, 2) = 27;

   Matrix B(4, 3);
   B(0, 0) = 1;
   B(0, 1) = 2;
   B(0, 2) = 3;
   B(1, 0) = 1;
   B(1, 1) = 4;
   B(1, 2) = 9;
   B(2, 0) = 1;
   B(2, 1) = 8;
   B(2, 2) = 27;
   B(3, 0) = 1;
   B(3, 1) = 16;
   B(3, 2) = 81;

   assert((A.multiplyTranspose(B) - (A * B.transpose())).abs().max() < 0.001);
}

void utest_createMatrix()
{
   Matrix A = Matrix::create(2, 2, 3.0);
   assert(A(0, 0) == 3);
   assert(A(0, 1) == 3);
   assert(A(1, 0) == 3);
   assert(A(1, 1) == 3);
}

void utest_determinant2()
{
   Matrix A(3, 3);
   A(0, 0) = 36.941682049885969;
   A(0, 1) = 41.987244479836477;
   A(0, 2) = 0.0013436097996784003;
   A(1, 0) = 41.987244479836477;
   A(1, 1) = 71.613303424117461;
   A(1, 2) = -0.0018905778065490752;
   A(2, 0) = 0.0013436097996784003;
   A(2, 1) = -0.0018905778065490752;
   A(2, 2) = 0.062161848085009568;

   double det;
   A.inverse((Matrix*) 0, &det);
   ::std::cerr << "Determinant : " << det << " vs " << A.determinant() << ::std::endl;
   assert(::std::abs(det - A.determinant()) < 0.0001);
}

void utest_externalBuffer()
{
   double data[] = {
         36.941682049885969, 41.987244479836477, 0.0013436097996784003, 41.987244479836477, 71.613303424117461,
         -0.0018905778065490752, 0.0013436097996784003, -0.0018905778065490752, 0.062161848085009568 };

   Matrix A = Matrix::createWithBuffer(3, 3, data);
   double det;
   A.inverse((Matrix*) 0, &det);
   ::std::cerr << "Determinant : " << det << " vs " << A.determinant() << ::std::endl;
   assert(::std::abs(det - A.determinant()) < 0.0001);

   Matrix CHECK_A(3, 3);
   CHECK_A(0, 0) = 36.941682049885969;
   CHECK_A(0, 1) = 41.987244479836477;
   CHECK_A(0, 2) = 0.0013436097996784003;
   CHECK_A(1, 0) = 41.987244479836477;
   CHECK_A(1, 1) = 71.613303424117461;
   CHECK_A(1, 2) = -0.0018905778065490752;
   CHECK_A(2, 0) = 0.0013436097996784003;
   CHECK_A(2, 1) = -0.0018905778065490752;
   CHECK_A(2, 2) = 0.062161848085009568;

   assert(CHECK_A.determinant() == A.determinant());
}

void utest_outerProduct()
{
   Vector u(1.0, 2.0);
   Vector v(3.0, 4.0);

   {
      Matrix M(u, v);
      assert(M(0, 0) == 3.0);
      assert(M(0, 1) == 4.0);
      assert(M(1, 0) == 6.0);
      assert(M(1, 1) == 8.0);
   }

   {
      Matrix M(u.dimension(), v.dimension());
      M.addOuterProduct(u, v);
      assert(M(0, 0) == 3.0);
      assert(M(0, 1) == 4.0);
      assert(M(1, 0) == 6.0);
      assert(M(1, 1) == 8.0);
   }
}

void utest_copy()
{
   {
      Matrix m(2, 2);
      m(1,1)=10;
      Matrix x(m);
      m(1,1)=100;
      assert(x.rowSize() == 2 && x.colSize() == 2);
      assert(m.rowSize() == 2 && m.colSize() == 2);
      assert(x(1,1)==10);
      assert(m(1,1)==100);
   }
   {
      Matrix m(2, 2);
      Matrix x;
      m(1,1)=10;
      x=m;
      m(1,1)=100;
      assert(x.rowSize() == 2 && x.colSize() == 2);
      assert(m.rowSize() == 2 && m.colSize() == 2);
      assert(x(1,1)==10);
      assert(m(1,1)==100);
   }
}

void utest_move()
{
   {
      Matrix m(2, 2);
      Matrix x(::std::move(m));
      assert(x.rowSize() == 2 && x.colSize() == 2);
      assert(m.rowSize() == 0 && m.colSize() == 0);
   }
   {
      Matrix m(2, 2);
      Matrix x(3, 3);
      x = ::std::move(m);
      assert(x.rowSize() == 2 && x.colSize() == 2);
      assert(m.rowSize() == 0 && m.colSize() == 0);
   }
}
