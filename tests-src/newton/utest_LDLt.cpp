#include <newton/LDLt.h>
#include <cmath>
#include <iostream>

using namespace ::std;

typedef ::newton::Matrix< > Matrix;
typedef ::newton::Vector< > Vector;
typedef ::newton::LDLt< > LDLt;

static LDLt checkLDLt(const Matrix& A)
{
   LDLt ch(A);
   LDLt invCh(ch.inverse());

   assert(A.isPositiveDefinite());

   const Matrix& L = ch.matrix();
   const Matrix& invL = invCh.matrix();
   const Matrix D = Matrix::diagonal(ch.diagonal());
   const Matrix invD = Matrix::diagonal(invCh.diagonal());

   Matrix Aprime = (L * D).multiplyTranspose(L);
   Matrix shouldBeI = (invL * invD).multiplyTranspose(invL) * (L.transpose() * D * L);
   ::std::cerr << "A   : " << A << endl << "A'  : " << Aprime << endl << "L   : " << L << endl << "D   : "
         << ch.diagonal() << endl << "A-A': " << (A - Aprime).abs() << ::std::endl << "Inv(L) : " << invL << ::std::endl
         << "Inv(L)*L : " << invL * L << ::std::endl << "Inv(L)*transpose(L) : " << invL.multiplyTranspose(L)
         << ::std::endl << "A*inv(A) : " << shouldBeI << ::std::endl;

   assert((A - Aprime).abs().max() < 1E-5);

   assert((invL * L - Matrix::identity(ch.matrix().rowSize())).abs().max() < 1E-5);
   assert((shouldBeI - Matrix::identity(ch.matrix().rowSize())).abs().max() < 1E-5);

   {
      LDLt tmpCH;
      Matrix LL = ch.matrix();
      Vector d = ch.diagonal();
      tmpCH.swap(LL, d);
      assert(tmpCH.determinant() == ch.determinant());
      tmpCH.assign(invL, invCh.diagonal());
      assert(tmpCH.determinant() == invCh.determinant());
   }

   return ch;
}

void utest_simpleLDLt1()
{
   Matrix A(1, 1);
   A(0, 0) = 25;
   LDLt ldlt = checkLDLt(A);
   const Matrix L = ldlt.matrix();
   assert(L(0, 0) == 1);
   assert(ldlt.diagonal()(0) == 25);
}

void utest_simpleLDLt2()
{
   Matrix A(4, 4);
   A(0, 0) = 4;
   A(1, 1) = 9;
   A(2, 2) = 16;
   A(3, 3) = 25;
   LDLt ldlt = checkLDLt(A);
   const Matrix L = ldlt.matrix();
   assert(ldlt.diagonal()(0) == 4);
   assert(ldlt.diagonal()(1) == 9);
   assert(ldlt.diagonal()(2) == 16);
   assert(ldlt.diagonal()(3) == 25);

   assert(L(0, 0) == 1);
   assert(L(1, 1) == 1);
   assert(L(2, 2) == 1);
   assert(L(3, 3) == 1);

}

void utest_simpleCholeksy3()
{
   Matrix A(2, 2);
   A(0, 0) = 4;
   A(0, 1) = A(1, 0) = 9;
   A(1, 1) = 25;

   LDLt ldlt = checkLDLt(A);
   const Matrix L = ldlt.matrix();
   assert(ldlt.diagonal()(0) == 4);
   assert(ldlt.diagonal()(1) == 4.75);

   assert(L(0, 0) == 1);
   assert(L(1, 0) == 2.25);
   assert(L(1, 1) == 1);
}

void utest_RotationMatrix()
{
   Matrix R(2, 2);
   double theta = M_PI / 45;

   R(0, 0) = cos(theta);
   R(0, 1) = sin(theta);
   R(1, 0) = -R(0, 1);
   R(1, 1) = R(0, 0);

   Matrix A(2, 2);
   A(0, 0) = 2;
   A(1, 1) = 6;

   Matrix P = (R * A).multiplyTranspose(R * A);

   LDLt ldlt = checkLDLt(P);

}

void utest_InvalidMatrix()
{
   Matrix A(2, 2);
   A(0, 0) = 4;
   A(1, 0) = A(0, 1) = 5;
   A(1, 1) = 2;

   try {
      LDLt ch(A);
   }
   catch (const ::std::exception& e) {
      assert(false);
   }

}

void utest_InnerProduct()
{
   Matrix A(3, 3);
   for (size_t i = 0; i < 3; ++i) {
      for (size_t j = 0; j <= i; ++j) {
         A(i, j) = A(j, i) = 1;
      }
   }

   A(0, 0) = 4;
   A(1, 1) = 9;
   A(2, 2) = 25;
   LDLt ch = checkLDLt(A);

   const Vector x(1.0, 2.0, 3.0);

   Vector y = ch.preMultiply(x);
   double ip1 = y * y;

   double ip2 = ch.innerProduct(x);
   double ip = x * A * x;

   ::std::cerr << "IP  " << ip << endl << "IP1 " << ip1 << endl << "IP2 " << ip2 << endl;
   assert(abs(ip1 - ip2) < 1E-8);
}

void utest_Solve()
{
   Matrix A(3, 3);
   for (size_t i = 0; i < 3; ++i) {
      for (size_t j = 0; j <= i; ++j) {
         A(i, j) = A(j, i) = 1;
      }
   }

   A(0, 0) = 4;
   A(1, 1) = 9;
   A(2, 2) = 25;
   LDLt ch = checkLDLt(A);
   const Vector b(1.0, 2.0, 3.0);
   Matrix B(3, 1);
   B(0, 0) = 1;
   B(1, 0) = 2;
   B(2, 0) = 3;

   Vector x1, x2;
   Matrix X1, X2;
   {
      Vector v(b);
      Vector u(3);
      Matrix V(B), U(3, 1);
      ch.doForwardSubstitution(v, u);
      for (size_t i = 0; i < u.dimension(); ++i) {
         u(i) /= ch.diagonal()(i);
      }
      ch.doBackwardSubstitution(u, v);
      ch.doForwardSubstitution(V, U);
      U = Matrix::diagonal(ch.diagonal()).inverse() * U;
      ch.doBackwardSubstitution(U, V);
      x1 = v;
      X1 = V;
   }

   {
      Vector v(b);
      Matrix V(B);
      ch.doForwardSubstitution(v, v);
      for (size_t i = 0; i < v.dimension(); ++i) {
         v(i) /= ch.diagonal()(i);
      }
      ch.doBackwardSubstitution(v, v);
      ch.doForwardSubstitution(V, V);
      V = Matrix::diagonal(ch.diagonal()).inverse() * V;
      ch.doBackwardSubstitution(V, V);
      x2 = v;
      X2 = V;
   }

   ::std::cerr << "x1 : " << x1 << endl << "Ax1-b " << (A * x1 - b).abs().max() << ::std::endl << "x2 : " << x2 << endl
         << "Ax2-b " << (A * x2 - b).abs().max() << ::std::endl;
   ::std::cerr << "X1 : " << X1 << endl << "AX1-B " << (A * X1 - B).abs().max() << ::std::endl << "X2 : " << X2 << endl
         << "AX2-B " << (A * X2 - B).abs().max() << ::std::endl << "AX2 " << (A * X2) << ::std::endl;
   assert(x1 == x2);
   assert(X1 == X2);
   assert((A * x2 - b).abs().max() < 1E-8);
   assert((A * X2 - B).abs().max() < 1E-8);

}

void utest_copy()
{
   {
      Matrix A(1, 1);
      A(0, 0) = 25;
      LDLt ldlt = checkLDLt(A);
      LDLt x(ldlt);
   }
   {
      LDLt x;
      Matrix A(1, 1);
      A(0, 0) = 25;
      LDLt ldlt = checkLDLt(A);
      x = ldlt;
   }
}
void utest_move()
{
   {
      Matrix A(1, 1);
      A(0, 0) = 25;
      LDLt ldlt = checkLDLt(A);
      LDLt x(::std::move(ldlt));
   }
   {
      LDLt x;
      Matrix A(1, 1);
      A(0, 0) = 25;
      LDLt ldlt = checkLDLt(A);
      x = ::std::move(ldlt);
   }
}
