#include <newton/Cholesky.h>
#include <cmath>
#include <iostream>

using namespace ::std;

typedef ::newton::Matrix< > Matrix;
typedef ::newton::Vector< > Vector;
typedef ::newton::Cholesky< > Cholesky;

static Cholesky checkCholesky(const Matrix& A)
{
   Cholesky ch(A);
   Cholesky invCh(ch.inverse());

   assert(A.isPositiveDefinite());

   const Matrix& L = ch.matrix();
   Matrix Aprime = L.multiplyTranspose(L);
   Matrix shouldBeI = invCh.matrix().multiplyTranspose(invCh.matrix()) * (L.transpose() * L);
   ::std::cerr << "A   : " << A << endl << "A'  : " << Aprime << endl << "L   : " << L << endl << "A-A': "
         << (A - Aprime).abs() << ::std::endl << "Inv(L) : " << invCh.matrix() << ::std::endl
         << "Inv(L)*transpose(L) : " << invCh.matrix().multiplyTranspose(ch.matrix()) << ::std::endl << "A*inv(A) : "
         << shouldBeI << ::std::endl;

   assert((A - Aprime).abs().max() < 1E-5);

   assert((invCh.matrix() * ch.matrix() - Matrix::identity(ch.matrix().rowSize())).abs().max() < 1E-5);
   assert((shouldBeI - Matrix::identity(ch.matrix().rowSize())).abs().max() < 1E-5);

   {
      Cholesky tmpCH;
      Matrix LL = ch.matrix();
      tmpCH.swap(LL);
      assert(tmpCH.determinant() == ch.determinant());
      tmpCH.assign(invCh.matrix());
      assert(tmpCH.determinant() == invCh.determinant());
   }

   return ch;
}

void utest_simpleCholesky1()
{
   Matrix A(1, 1);
   A(0, 0) = 25;
   const Matrix L = checkCholesky(A).matrix();
   assert(L(0, 0) == 5);
}

void utest_simpleCholesky2()
{
   Matrix A(4, 4);
   A(0, 0) = 4;
   A(1, 1) = 9;
   A(2, 2) = 16;
   A(3, 3) = 25;
   const Matrix L = checkCholesky(A).matrix();
   assert(L(0, 0) == 2);
   assert(L(1, 1) == 3);
   assert(L(2, 2) == 4);
   assert(L(3, 3) == 5);

}

void utest_simpleCholeksy3()
{
   Matrix A(2, 2);
   A(0, 0) = 4;
   A(0, 1) = A(1, 0) = 9;
   A(1, 1) = 25;

   const Matrix L = checkCholesky(A).matrix();
   assert(L(0, 0) == 2);
   assert(L(1, 0) == 4.5);
   assert(L(1, 1) == ::std::sqrt(4.75));
}

void utest_RotationMatrix()
{
   Matrix R(2, 2);
   double theta = M_PI / 45;

   R(0, 0) = cos(theta);
   R(0, 1) = sin(theta);
   R(1, 0) = -R(0, 1);
   R(1, 1) = R(0, 0);

   Matrix A(2, 2);
   A(0, 0) = 2;
   A(1, 1) = 6;

   Matrix P = (R * A).multiplyTranspose(R * A);

   checkCholesky(P).matrix();

}

void utest_InvalidMatrix()
{
   Matrix A(2, 2);
   A(0, 0) = 4;
   A(1, 0) = A(0, 1) = 5;
   A(1, 1) = 2;

   try {
      Cholesky ch(A);
      assert(false);
   }
   catch (const ::std::exception& e) {
      cerr << "Exception " << e.what() << endl;
   }

}

void utest_InnerProduct()
{
   Matrix A(3, 3);
   for (size_t i = 0; i < 3; ++i) {
      for (size_t j = 0; j <= i; ++j) {
         A(i, j) = A(j, i) = 1;
      }
   }

   A(0, 0) = 4;
   A(1, 1) = 9;
   A(2, 2) = 25;
   Cholesky ch = checkCholesky(A);

   const Vector x(1.0, 2.0, 3.0);

   Vector y = ch.preMultiply(x);
   double ip1 = y * y;

   double ip2 = ch.innerProduct(x);
   double ip = x * A * x;

   ::std::cerr << "IP  " << ip << endl << "IP1 " << ip1 << endl << "IP2 " << ip2 << endl;
   assert(abs(ip1 - ip2) < 1E-8);
}

void utest_Solve()
{
   Matrix A(3, 3);
   for (size_t i = 0; i < 3; ++i) {
      for (size_t j = 0; j <= i; ++j) {
         A(i, j) = A(j, i) = 1;
      }
   }

   A(0, 0) = 4;
   A(1, 1) = 9;
   A(2, 2) = 25;
   Cholesky ch = checkCholesky(A);
   const Vector b(1.0, 2.0, 3.0);
   Matrix B(3, 1);
   B(0, 0) = 1;
   B(1, 0) = 2;
   B(2, 0) = 3;

   Vector x1, x2;
   Matrix X1, X2;
   {
      Vector v(b);
      Vector u(3);
      Matrix V(B), U(3, 1);
      ch.doForwardSubstitution(v, u);
      ch.doBackwardSubstitution(u, v);
      ch.doForwardSubstitution(V, U);
      ch.doBackwardSubstitution(U, V);
      x1 = v;
      X1 = V;
   }

   {
      Vector v(b);
      Matrix V(B);
      ch.doForwardSubstitution(v, v);
      ch.doBackwardSubstitution(v, v);
      ch.doForwardSubstitution(V, V);
      ch.doBackwardSubstitution(V, V);
      x2 = v;
      X2 = V;
   }

   ::std::cerr << "x1 : " << x1 << endl << "Ax1-b " << (A * x1 - b).abs().max() << ::std::endl << "x2 : " << x2 << endl
         << "Ax2-b " << (A * x2 - b).abs().max() << ::std::endl;
   ::std::cerr << "X1 : " << X1 << endl << "AX1-B " << (A * X1 - B).abs().max() << ::std::endl << "X2 : " << X2 << endl
         << "AX2-B " << (A * X2 - B).abs().max() << ::std::endl << "AX2 " << (A * X2) << ::std::endl;
   assert(x1 == x2);
   assert(X1 == X2);
   assert((A * x2 - b).abs().max() < 1E-8);
   assert((A * X2 - B).abs().max() < 1E-8);

}

void utest_copy()
{
   {
      Matrix A(1, 1);
      A(0, 0) = 25;
      const Cholesky tmp(checkCholesky(A));
      Cholesky ch(tmp);

   }
   {
      Cholesky ch;
      Matrix A(1, 1);
      A(0, 0) = 25;
      ch = checkCholesky(A);
   }

}
void utest_move()
{
   {
      Matrix A(1, 1);
      A(0, 0) = 25;
      auto tmp = checkCholesky(A);
      Cholesky ch(::std::move(tmp));
   }
   {
      Cholesky ch;
      Matrix A(1, 1);
      A(0, 0) = 25;
      ch = checkCholesky(A);
   }

}
