#ifndef _NEWTON_LDLT_H
#define _NEWTON_LDLT_H

#ifndef _NEWTON_MATRIX_H
#include <newton/Matrix.h>
#endif

namespace newton {

   /**
    * This class decomposes a symmetric matrix into
    * a lower, a diagonal, and upper triangular matrices. The two triangular matrices are transposes
    * of each other. This decomposition is essentially the same as the Cholesky, but does not
    * require the matrix to be positive-definite.
    */
   template<class T = double> class LDLt
   {
         /**
          * A default constructor
          */
      public:
         inline LDLt()
         {
         }

         /**
          * A copy constructor.
          */
      public:
         inline LDLt(const LDLt& src) = default;

         /**
          * A move constructor.
          */
      public:
         inline LDLt(LDLt&& src) throws()
               : _matrix(::std::move(src._matrix)), _diag(::std::move(src._diag))
         {
         }

         /**
          * A copy operator.
          */
      public:
         inline LDLt& operator=(const LDLt& src) = default;

         /**
          * A copy operator.
          */
      public:
         LDLt& operator=(LDLt&& src) throws()
         {
            _matrix = ::std::move(src._matrix);
            _diag = ::std::move(src._diag);
            return *this;
         }

         /**
          * The default constructor.
          * @param sz the size of the matrix
          */
      private:
         LDLt(size_t n)
               : _matrix(n, n), _diag(n)
         {
         }

         /**
          * Create a LDLt decomposition of the specified matrix.
          * @param A a symmetric  matrix
          * @throws ::std::exception if the matrix is not symmetric
          */
      public:
         LDLt(const Matrix< T>& A) throws (::std::exception)
               : _matrix(A.rowSize(), A.colSize()), _diag(A.rowSize())
         {
            if (!A.isSquare()) {
               throw ::std::invalid_argument("Not a square matrix");
            }

            const size_t nr = A.rowSize();
            const size_t nc = A.colSize();
            for (size_t k = 0; k < nc; ++k) {
               // compute the diagonal entry
               {
                  T tmp = A(k, k);
                  for (size_t j = 0; j < k; ++j) {
                     const double& l_kj = _matrix(k, j);
                     tmp -= l_kj * l_kj * _diag(j);
                  }
                  _diag(k) = tmp;
                  _matrix(k, k) = 1;
               }

               // compute the lower-diagonal entries
               for (size_t i = k + 1; i < nr; ++i) {
                  T tmp = A(i, k);
                  if (A(k, i) != tmp) {
                     throw ::std::invalid_argument("Matrix is not symmetric");
                  }
                  for (size_t j = 0; j < k; ++j) {
                     tmp -= _matrix(i, j) * _matrix(k, j) * _diag(j);
                  }
                  tmp /= _diag(k);
                  _matrix(i, k) = tmp;
               }
            }
         }

         /** The destructor */
      public:
         ~LDLt() throws()
         {
         }

         /**
          * Check that the specified matrix is indeed lower-triangular
          * with a 1's across the diagonal.
          * @param L a lower triangular matrix
          * @param D a vector of the same dimension as L and with no 0 entries.
          * @return if L and D a possible decompositions
          */
      public:
         static bool isLD(const Matrix< T>& L, const Vector< T>& D) throws()
         {
            if (!L.isSquare() && D.dimension() == L.rowSize()) {
               return false;
            }
            for (size_t i = 0, sz = L.rowSize(); i < sz; ++i) {
               if (D(i) == 0 || L(i, i) != 1) {
                  return false;
               }

               for (size_t j = 0; j < i; ++j) {
                  if (L(j, i) != 0) { // upper triangular matrix is not 0
                     return false;
                  }
               }
            }
            return true;
         }

         /**
          * Assign the lower-triangular matrix to this decomposition.
          * @param L a lower triangular matrix, whose diagonal entries are all 1.
          * @param D a vector with of the same dimension as L and no 0 entries
          * @pre isLD(L,D)
          */
      public:
         void assign(const Matrix< T>& L, const Vector< T>& D) throws (::std::exception)
         {
            assert(isLD(L, D));
            _matrix = L;
            _diag = D;
         }

         /**
          * Swap the cholesky matrix with the specified matrix. This method
          * may be implemented by calling the swap method on the cholesky
          * and therefore be more efficient than using assignment.
          *
          * @param L a lower triangular matrix
          * @param D a diagonal matrix, represented as a vector
          * @pre isLD(L,D)
          */
      public:
         void swap(Matrix< T>& L, Vector< T>& D) throws (::std::exception)
         {
            assert(isLD(L, D));
            _matrix.swap(L);
            _diag.swap(D);
         }

         /**
          * Get the lower triangular matrix L of the decomposition.
          * @return the lower-triangular matrix L
          */
      public:
         inline const Matrix< T>& matrix() const throws()
         {
            return _matrix;
         }

         /**
          * Get the diagonal entries of the decomposition.
          * @return the vector representing the diagonal entries (i,i)
          */
      public:
         inline const Vector< T>& diagonal() const throws()
         {
            return _diag;
         }

         /**
          * Get the original matrix. This method performs a matrix multiply
          * to reconstitute the original matrix. The result will not exactly
          * be the same as the original matrix due to round-off errors.
          * @return matrix().multiplyTranspose(matrix())
          */
      public:
         Matrix< T> originalMatrix() const throws()
         {
            Matrix< T> m(_matrix.rowSize(), _matrix.colSize(), 0.0);
            for (size_t i = 0, sz = _matrix.rowSize(); i < sz; ++i) {
               for (size_t j = i; j < sz; ++i) {
                  m(i, j) = _diag(i) * _matrix(j, i);
               }
            }
            return _matrix * m;
         }

         /**
          * Get the determinant of matrix of diagonal elements. The determinant
          * of the original matrix A is this determinant. If this
          * method returns 0, then the cholesky has not been performed.
          * @return the determinant of matrix()
          */
      public:
         T determinant() const throws()
         {
            size_t sz = _diag.dimension();
            if (sz == 0) {
               return 0;
            }
            double det = 1;
            for (size_t i = 0; i < sz; ++i) {
               det *= _diag(i);
            }
            return det;
         }

         /**
          * Create a LDLt decomposition that is the inverse of this decomposition.
          * @return the inverse of this decomposition
          */
      public:
         LDLt inverse() const throws()
         {
            size_t n = _matrix.rowSize();
            LDLt inv(n);
            // do forward substitution k times
            for (size_t k = 0; k < n; ++k) {
               for (size_t i = k; i < n; ++i) {
                  T tmp = (i == k ? 1 : 0);
                  for (size_t j = 0; j < i; ++j) {
                     tmp -= _matrix(i, j) * inv._matrix(j, k);
                  }
                  inv._matrix(i, k) = tmp;
               }
               inv._diag(k) = 1.0 / _diag(k);
            }
            return inv;
         }

         /**
          * @name Solving systems of equations.
          * To solve a system of equations of the form Ax=b, use Cholesky decomposition as follows:
          * @code
          *   Cholesky ch(A);
          *   Vector y,x;
          *   ch.doForwardSubstitution(b,y);
          *   ch.doBackwardSubstitution(y,x);
          * @endcode
          * Using both forward and backward substitution, one can multiply
          * a vector (or matrix) by the inverse of the lower or upper triangular matrix, respectively.
          * Using this, one can quickly calculate the following:
          * @code d = x*A.inverse().x@endcode
          *  using backward substitution
          * @code
          *   Cholesky ch(A);
          *   Vector tmp;
          *   ch.doBackwardSubstitution(x,tmp);
          *   d= tmp*tmp;
          * @endcode
          * or forward substitution
          * @code
          *   Cholesky ch(A);
          *   Vector tmp;
          *   ch.doForwardSubstitution(x,tmp);
          *   d= tmp*tmp;
          * @endcode
          */

         /**
          * Perform the forward substitution algorithm on a vector. Forward substitution
          * is equivalent to matrix().inverse()*v.
          * @note it is explicitly allowed to have &u == &v.
          * @param v a vector
          * @param u the result vector
          */
      public:
         void doForwardSubstitution(const Vector< T>& v, Vector< T>& u) const throws (::std::exception)
         {
            size_t sz = v.dimension();
            if (sz != _matrix.rowSize()) {
               throw ::std::invalid_argument("Invalid vector dimension");
            }
            u(0) = v(0);

            for (size_t i = 1; i < sz; ++i) {
               T tmp = v(i);
               for (size_t j = 0; j < i; ++j) {
                  tmp -= _matrix(i, j) * u(j);
               }
               u(i) = tmp;
            }
         }

         /**
          * Perform the forward substitution algorithm on a matrix. This
          * function is equivalent to matrix().inverse()*v.
          * @note it is explicitly allowed to have &u == &v.
          * @param v a vector
          * @param u the result vector
          */
      public:
         void doForwardSubstitution(const Matrix< T>& v, Matrix< T>& u) const throws (::std::exception)
         {
            size_t sz = v.rowSize();
            if (sz != _matrix.rowSize()) {
               throw ::std::invalid_argument("Invalid vector dimension");
            }
            if (v.colSize() != u.colSize()) {
               throw ::std::invalid_argument("Incompatible matrix column sizes");
            }
            for (size_t k = 0; k < v.colSize(); ++k) {
               u(0, k) = v(0, k);
               for (size_t i = 1; i < sz; ++i) {
                  T tmp = v(i, k);
                  for (size_t j = 0; j < i; ++j) {
                     tmp -= _matrix(i, j) * u(j, k);
                  }
                  u(i, k) = tmp;
               }
            }
         }

         /**
          * Perform the backward substitution algorith on the transpose of the Cholesky matrix.
          * Backward substitution is equivalent to v*matrix().transpose().inverse().
          * @note it is explicitly allowed to have &u == &v.
          * @param v a vector
          * @param u the result vector
          */
      public:
         void doBackwardSubstitution(const Vector< T>& v, Vector< T>& u) const throws (::std::exception)
         {
            size_t sz = v.dimension();
            if (sz != _matrix.rowSize()) {
               throw ::std::invalid_argument("Invalid vector dimension");
            }
            size_t i = sz - 1;
            u(i) = v(i);

            while (i-- > 0) {
               T tmp = v(i);
               for (size_t j = i + 1; j < sz; ++j) {
                  tmp -= u(j) * _matrix(j, i); // remember transposed matrix!
               }
               u(i) = tmp;
            }
         }

         /**
          * Perform the backward substitution algorith on the transpose of the Cholesky matrix.
          * @note it is explicitly allowed to have &u == &v.
          * @param v a matrix
          * @param u the result matrix
          */
      public:
         void doBackwardSubstitution(const Matrix< T>& v, Matrix< T>& u) const throws (::std::exception)
         {
            size_t sz = v.rowSize();
            if (sz != _matrix.rowSize()) {
               throw ::std::invalid_argument("Invalid vector dimension");
            }
            if (v.colSize() != u.colSize()) {
               throw ::std::invalid_argument("Incompatible matrix column sizes");
            }

            for (size_t k = 0; k < v.colSize(); ++k) {
               size_t i = sz - 1;
               u(i, k) = v(i, k);

               while (i-- > 0) {
                  T tmp = v(i, k);
                  for (size_t j = i + 1; j < sz; ++j) {
                     tmp -= u(j, k) * _matrix(j, i); // remember transposed matrix!
                  }
                  u(i, k) = tmp;
               }
            }
         }

         /**
          * Pre-multiply the lower-triangular matrix by v.
          * @param v a vector
          * @return a new vector
          */
      public:
         Vector< T> preMultiply(const Vector< T>& v) const throws ( ::std::exception)
         {
            size_t sz = v.dimension();
            Vector< T> u(sz);
            for (size_t i = 0; i < sz; ++i) {
               T tmp = 0;
               for (size_t j = i; j < sz; ++j) {
                  tmp += v(j) * _matrix(j, i);
               }
               u(i) = tmp;
            }
            return u;
         }

         /**
          * Compute the inner product of the vector matrix().transpose()*v.
          * This calculation is fairly efficient and requires no extra storage.
          * @param v a vector
          * @return a scalar
          */
      public:
         T innerProduct(const Vector< T>& v) const throws ( ::std::exception)
         {
            size_t sz = v.dimension();
            T ip = 0;
            for (size_t i = 0; i < sz; ++i) {
               T tmp = 0;
               for (size_t j = i; j < sz; ++j) {
                  tmp += v(j) * _matrix(j, i);
               }
               ip += tmp * tmp;
            }
            return ip;
         }

         /**
          * Swap this decomposition with another.
          * @param ch a LDLt decomposition
          */
      public:
         inline void swap(LDLt& ch) throws()
         {
            _matrix.swap(ch._matrix);
            _diag.swap(ch._diag);
         }

         /** The lower-triangular matrix  */
      private:
         Matrix< T> _matrix;

         /** The diagonal entry */
      private:
         Vector< T> _diag;

   };

}

#endif
