#ifndef _NEWTON_LAPACK_H
#define _NEWTON_LAPACK_H

namespace newton {

   /**
    * The class providing the namespace for LAPACK.
    */
  class LAPACK {

     private:
	LAPACK();
        LAPACK(const LAPACK&);
        LAPACK&operator=(const LAPACK&);
        ~LAPACK();



   };
}
#endif
