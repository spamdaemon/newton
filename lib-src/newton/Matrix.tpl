#ifndef _NEWTON_MATRIX_TPL
#define _NEWTON_MATRIX_TPL

#ifndef _NEWTON_MATRIX_H
#include <newton/Matrix.h>
#endif

#ifndef _CANOPY_ARRAYCOPY_H
#include <canopy/arraycopy.h>
#endif

#include <cmath>
#include <vector>
#include <stdexcept>
#include <iostream>
#include <algorithm>

namespace newton {

   template<class T>
   Matrix< T>::Matrix(Matrix&& source) throws()
         : _rowSize(source._rowSize), _colSize(source._colSize), _dataBuffer(source._dataBuffer), _data(source._data)
   {
      source._rowSize = source._colSize = 0;
      source._dataBuffer = source._data = nullptr;
   }

   template<class T>
   Matrix< T>& Matrix< T>::operator=(Matrix&& source) throws()
   {
      if (this != &source) {
         delete[] _dataBuffer;
         _rowSize = source._rowSize;
         _colSize = source._colSize;
         _dataBuffer = source._dataBuffer;
         _data = source._data;
         source._rowSize = source._colSize = 0;
         source._dataBuffer = source._data = nullptr;
      }
      return *this;
   }

   template<class T>
   Matrix< T>& Matrix< T>::operator=(const Matrix& source) throws()
   {
      if (_data != source._data) {
         size_t sizeThis = _rowSize * _colSize;
         size_t size = source._rowSize * source._colSize;

         // delete the old data if the size of the array has changed
         if (sizeThis > 0 && sizeThis != size) {
            delete[] _dataBuffer;
            _data = _dataBuffer = 0;
         }
         _rowSize = source._rowSize;
         _colSize = source._colSize;

         // if we still have an array we don't need to allocate
         // anything
         if (_data == 0 && size > 0) {
            _dataBuffer = _data = new T[size];
         }

         // copy the new data into this array
         ::std::copy(source._data, source._data + size, _data);
      }
      return *this;
   }

   template<class T>
   void Matrix< T>::initialize(size_t r, size_t c, const Matrix* data) throws()
   {
      assert((r > 0 && c > 0) || (r == 0 && c == 0));
      _rowSize = r;
      _colSize = c;
      _dataBuffer = _data = 0;
      size_t sz = r * c;
      if (sz > 0) {
         _dataBuffer = _data = new T[sz];
         if (data != 0) {
            ::std::copy(data->_data, data->_data + sz, _data);
         }
         else {
            ::std::fill(_data, _data + sz, 0);
         }
      }
   }

   template<class T>
   void Matrix< T>::multiply(const Matrix& m, Matrix& res) const throws()
   {
      assert(m.rowSize() == colSize());

      if (res._rowSize * res._colSize != _rowSize * m._colSize) {
         Matrix(_rowSize, m._colSize).swap(res);
      }
      res._rowSize = _rowSize;
      res._colSize = m._colSize;
      for (size_t k = 0; k < m.colSize(); ++k) {
         for (size_t r = 0; r < rowSize(); ++r) {
            const T* current = getRow(r);
            T& tmp = res(r, k);
            tmp = current[0] * m(0, k);
            for (size_t c = 1; c < colSize(); ++c) {
               tmp += current[c] * m(c, k);
            }
         }
      }
   }

   template<class T>
   T Matrix< T>::min() const throws()
   {
      assert(_data != 0);
      return *::std::min_element(_data, _data + rowSize() * colSize());
   }

   template<class T>
   T Matrix< T>::max() const throws()
   {
      assert(_data != 0);
      return *::std::max_element(_data, _data + rowSize() * colSize());
   }

   template<class T>
   Matrix< T> Matrix< T>::abs() const throws()
   {
      Matrix< T> res(*this);
      T* end = res._data + (rowSize() * colSize());
      for (T* i = res._data; i < end; ++i) {
         *i = ::std::abs(*i);
      }
      return res;
   }

   template<class T>
   void Matrix< T>::preMultiply(const Vector< T>& v, Vector< T>& res) const throws()
   {
      assert(v.dimension() == colSize());
      if (res.dimension() != rowSize()) {
         Vector< T>(rowSize()).swap(res);
      }

      for (size_t r = 0; r < rowSize(); ++r) {
         const T* current = getRow(r);
         T tmp(current[0] * v(0));
         for (size_t c = 1; c < colSize(); ++c) {
            tmp += current[c] * v(c);
         }
         res.set(r, tmp);
      }
   }

   template<class T>
   Vector< T> Matrix< T>::preMultiply(const Vector< T>& v) const throws()
   {
      Vector< T> res(rowSize());
      preMultiply(v, res);
      return res;
   }

   template<class T>
   void Matrix< T>::postMultiply(const Vector< T>& v, Vector< T>& res) const throws()
   {
      assert(v.dimension() == rowSize());
      if (res.dimension() != colSize()) {
         Vector< T>(colSize()).swap(res);
      }
      for (size_t c = 0; c < colSize(); ++c) {
         T tmp(get(0, c) * v(0));
         for (size_t r = 1; r < rowSize(); ++r) {
            tmp += get(r, c) * v(r);
         }
         res.set(c, tmp);
      }
   }

   template<class T>
   Vector< T> Matrix< T>::postMultiply(const Vector< T>& v) const throws()
   {
      Vector< T> res(colSize());
      postMultiply(v, res);
      return res;
   }

   template<class T>
   bool Matrix< T>::equals(const Matrix& m) const throws()
   {
      if (rowSize() != m.rowSize()) {
         return false;
      }
      if (colSize() != m.colSize()) {
         return false;
      }
      const T* end = _data + colSize() * rowSize();
      for (const T* i = _data, *j = m._data; i < end; ++i, ++j) {
         if ((*i) != (*j)) {
            return false;
         }
      }
      return true;
   }

   template<class T>
   Matrix< T> Matrix< T>::transpose() const throws()
   {
      Matrix res(colSize(), rowSize());
      for (size_t i = 0; i < rowSize(); ++i) {
         for (size_t j = 0; j < colSize(); ++j) {
            res(j, i) = get(i, j);
         }
      }
      return res;
   }

   template<class T>
   void Matrix< T>::multiplyTranspose(const Matrix& m, Matrix& res) const throws()
   {
      assert(m.colSize() == colSize());
      if (res._rowSize * res._colSize != _rowSize * m._rowSize) {
         Matrix(_rowSize, m._rowSize).swap(res);
      }
      // ensure that the sizes are the same even if the row*col has the expected value
      res._rowSize = _rowSize;
      res._colSize = m._rowSize;
      for (size_t k = 0; k < m.rowSize(); ++k) {
         for (size_t r = 0; r < rowSize(); ++r) {
            const T* current = getRow(r);
            T& tmp = res(r, k);
            tmp = current[0] * m(k, 0);
            for (size_t c = 1; c < colSize(); ++c) {
               tmp += current[c] * m(k, c);
            }
         }
      }
   }

   template<class T>
   void Matrix< T>::transposeMatrix() throws()
   {
      Matrix res(colSize(), rowSize());
      for (size_t i = 0; i < rowSize(); ++i) {
         for (size_t j = 0; j < colSize(); ++j) {
            res(j, i) = get(i, j);
         }
      }
      swap(res);
   }

   template<class T>
   void Matrix< T>::negate() throws()
   {
      const T* end = _data + (rowSize() * colSize());
      for (T* i = _data; i < end; ++i) {
         *i = -(*i);
      }
   }

   template<class T>
   void Matrix< T>::scale(const T& s) throws()
   {
      const T* end = _data + (rowSize() * colSize());
      for (T* i = _data; i < end; ++i) {
         *i *= s;
      }
   }

   template<class T>
   void Matrix< T>::invScale(const T& s) throws()
   {
      const T* end = _data + (rowSize() * colSize());
      for (T* i = _data; i < end; ++i) {
         *i /= s;
      }
   }

   template<class T>
   void Matrix< T>::add(const Matrix& v) throws()
   {
      assert(rowSize() == v.rowSize());
      assert(colSize() == v.colSize());
      const T* end = _data + (rowSize() * colSize());
      for (T* i = _data, *j = v._data; i < end; ++i, ++j) {
         (*i) += (*j);
      }
   }

   template<class T>
   void Matrix< T>::sub(const Matrix& v) throws()
   {
      assert(rowSize() == v.rowSize());
      assert(colSize() == v.colSize());
      const T* end = _data + (rowSize() * colSize());
      for (T* i = _data, *j = v._data; i < end; ++i, ++j) {
         (*i) -= (*j);
      }
   }

   template<class T>
   void Matrix< T>::add(const T& s, const Matrix& v) throws()
   {
      assert(rowSize() == v.rowSize());
      assert(colSize() == v.colSize());
      const T* end = _data + (rowSize() * colSize());
      for (T* i = _data, *j = v._data; i < end; ++i, ++j) {
         (*i) += s * (*j);
      }
   }

   template<class T>
   void Matrix< T>::sub(const T& s, const Matrix& v) throws()
   {
      assert(rowSize() == v.rowSize());
      assert(colSize() == v.colSize());
      const T* end = _data + (rowSize() * colSize());
      for (T* i = _data, *j = v._data; i < end; ++i, ++j) {
         (*i) -= s * (*j);
      }
   }

   template<class T>
   T Matrix< T>::determinant() const throws()
   {
      assert(isSquare() && "Matrix is not square");
      const size_t n = rowSize();

      Matrix M(*this);

      T det = 1;
      // setup the pivots for gaussian elimination
      ::std::vector < size_t > p; // the pivots
      p.reserve(rowSize());
      for (size_t i = 0; i < rowSize(); ++i) {
         p.push_back(i);
      }

      // forward gaussian elimination
      {
         // reduce this matrix using gaussian elimination
         for (size_t k = 0; k < n; ++k) {
            // find the best entry (aka the pivot)
            size_t pdx = k;
            T pivot(::std::abs(M(p[k], k)));

            for (size_t i = k + 1; i < n; ++i) {
               const T t(::std::abs(M(p[i], k)));
               if (pivot < t) {
                  pdx = i;
                  pivot = t;
               }
            }

            // got a pivot, but if it's sign is 0
            // return unsuccessfully
            const T eps = 0;
            if (::std::abs(pivot) <= eps) {
               return 0;
            }
            // swap the pivots to that p[k] = p[pdx]
            // and p[pdx] = p[k]
            if (k != pdx) {
               ::std::swap(p[k], p[pdx]);
               det = -det;
            }

            T* pRowStart = M.getRow(p[k]) + k;
            det *= *pRowStart;

            const T* pRowEnd = M.getRow(p[k]) + n;

            // subtract the remaining n-k rows from p[k]
            for (size_t i = k + 1; i < n; ++i) {
               const T* pRow = pRowStart;
               T* iRow = M.getRow(p[i]) + k;

               *iRow /= *pRowStart;
               const T m = *iRow;
               while (++pRow < pRowEnd) {
                  ++iRow;
                  *iRow -= m * (*pRow);
               }
            }
         }
      }
      return det;
   }

   template<class T>
   bool Matrix< T>::isPositiveDefinite() const throws (::std::invalid_argument)
   {
      if (!isSquare()) {
         throw ::std::invalid_argument("Not a square matrix");
      }
      if (get(0, 0) <= 0.0) {
         return false;
      }
      double det = determinant();
      if (det <= 0.0) {
         return false;
      }

      for (size_t k = 2, n = rowSize(); k < n; ++k) {
         Matrix S(k);
         for (size_t i = 0; i < k; ++i) {
            for (size_t j = 0; j < k; ++j) {
               S(i, j) = get(i, j);
            }
         }
         det = S.determinant();
         if (det <= 0.0) {
            return false;
         }
      }
      return true;
   }

   template<class T>
   T Matrix< T>::invert(const Matrix* rightMatrix) throws (::std::exception)
   {
      assert(isSquare() && "Matrix is not square");

      const size_t n = rowSize();

      // the resulting matrix
      Matrix I(rightMatrix ? *rightMatrix : identity(n));
      const size_t nI = I.colSize();

      assert(I.rowSize() == colSize());
      T det = 1;

      // setup the pivots for gaussian elimination
      {
         ::std::vector < size_t > p; // the pivots
         p.reserve(rowSize());
         for (size_t i = 0; i < rowSize(); ++i) {
            p.push_back(i);
         }

         // forward gaussian elimination
         {
            // reduce this matrix using gaussian elimination
            for (size_t k = 0; k < n; ++k) {
               // find the best entry (aka the pivot)
               size_t pdx = k;
               T pivot(::std::abs(get(p[k], k)));

               for (size_t i = k + 1; i < n; ++i) {
                  const T t(::std::abs(get(p[i], k)));
                  if (pivot < t) {
                     pdx = i;
                     pivot = t;
                  }
               }

               // got a pivot, but if it's sign is 0
               // return unsuccessfully
               const T eps = 0;
               if (::std::abs(pivot) <= eps) {
                  throw ::std::runtime_error("Matrix is singular");
               }
               // swap determinant due to row swap
               if (k != pdx) {
                  det = -det;
               }

               // swap the pivots to that p[k] = p[pdx]
               // and p[pdx] = p[k]
               ::std::swap(p[k], p[pdx]);

               T* pRowStart = getRow(p[k]) + k;
               const T* pRowEnd = getRow(p[k]) + n;
               det *= *pRowStart;

               // subtract the remaining n-k rows from p[k]
               for (size_t i = k + 1; i < n; ++i) {
                  const T* pRow = pRowStart;
                  T* iRow = getRow(p[i]) + k;

                  *iRow /= *pRowStart;
                  const T m = *iRow;
                  while (++pRow < pRowEnd) {
                     ++iRow;
                     *iRow -= m * (*pRow);
                  }

                  for (size_t j = 0; j < nI; ++j) {
                     I(p[i], j) -= m * I(p[k], j);
                  }
               }
            }
         }

         // backward gaussian elimination
         {
            // no more pivoting, though
            for (size_t k = n; k-- > 0;) {
               const T piv = get(p[k], k);
               T* pRow = I.getRow(p[k]);
               const T* const pRowEnd = pRow + nI;
               while (pRow < pRowEnd) {
                  *pRow /= piv;
                  ++pRow;
               }
               for (size_t i = k; i-- > 0;) {
                  T* iRow = I.getRow(p[i]);
                  pRow = I.getRow(p[k]);
                  while (pRow < pRowEnd) {
                     (*iRow) -= get(p[i], k) * (*pRow);
                     ++pRow;
                     ++iRow;
                  }
               }
            }
         }

         // restore the rows in I
         for (size_t k = 0; k < n; ++k) {
            if (p[k] != k) {
               for (size_t i = k + 1; i < n; ++i) {
                  if (p[i] == k) {
                     T* pi = I.getRow(p[i]);
                     const T* end = pi + nI;
                     T* pk = I.getRow(p[k]);

                     while (pi < end) {
                        ::std::swap(*pi, *pk);
                        ++pi;
                        ++pk;
                     }
                     ::std::swap(p[i], p[k]);
                     break;
                  }
               }
            }
         }
      }
      swap(I);
      return det;
   }
}

template<class T>
::std::ostream& operator<<(::std::ostream& out, const ::newton::Matrix< T>& v) throws()
{
   out << "Dimension : " << v.rowSize() << 'x' << v.colSize() << ::std::endl;
   for (size_t i = 0; i < v.rowSize(); ++i) {
      out << "|";
      for (size_t j = 0; j < v.colSize(); ++j) {
         out << '\t' << v(i, j);
      }
      out << " |" << ::std::endl;
   }
   return out;
}

#endif

