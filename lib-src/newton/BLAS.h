#ifndef _NEWTON_BLAS_H
#define _NEWTON_BLAS_H

namespace newton {

   /**
    * The class providing the namespace for BLAS.
    */
   class BLAS {

     private:
	BLAS();
        BLAS(const BLAS&);
        BLAS&operator=(const BLAS&);
        ~BLAS();

   };
}
#endif
