#ifndef _NEWTON_VECTOR_H
#define _NEWTON_VECTOR_H

#ifndef _NEWTON_H
#include <newton/newton.h>
#endif

#include <cmath>
#include <cassert>
#include <iosfwd>

namespace newton {
   /**
    * This vector is an implementation of a vector
    * using arbitrary values.
    */
   template<class T = double> class Vector
   {
         /**
          * The name of the type
          */
      public:
         typedef T ValueType;

         /**
          * @name Vector traits definitions
          * @{
          */

         /**
          * Satisfies traits for the vectors
          */
      public:
         typedef Vector Type;

         /**
          * The vector is its own difference type.
          */
      public:
         typedef Vector Difference;

         /*@}*/

         /**
          * Create a new vector in the specified dimension. The
          * vector is initialized to 0
          */
      public:
         inline Vector() throws()
               : _vector(nullptr), _dataBuffer(nullptr), _dim(0)
         {
         }

         /**
          * Create a new vector in the specified dimension. The
          * vector is initialized to 0
          */
      public:
         inline Vector(::std::nullptr_t) throws()
               : _vector(nullptr), _dataBuffer(nullptr), _dim(0)
         {
         }

         /**
          * Create a new vector in the specified dimension. The
          * vector is initialized to 0
          * @param dim
          */
      public:
         explicit Vector(size_t dim) throws();

         /**
          * Create a new vector in the specified dimension. The
          * vector is initialized to defaultValue.
          * @param dim
          * @param defaultValue the value for each entry
          */
      public:
         static Vector create(size_t dim, const T& defaultValue) throws();

         /**
          * Create a vector in 2D.
          * @param x the x-coordinate
          * @param y the y-coordinate
          */
      public:
         Vector(const T& x, const T& y) throws();

         /**
          * Create a vector in 3D.
          * @param x the x-coordinate
          * @param y the y-coordinate
          * @param z the z-coordinate
          */
      public:
         Vector(const T& x, const T& y, const T& z) throws();

         /**
          * Create a vector in 4D.
          * @param u the u-coordinate
          * @param x the x-coordinate
          * @param y the y-coordinate
          * @param z the z-coordinate
          */
      public:
         Vector(const T& u, const T& x, const T& y, const T& z) throws();

         /**
          * Create a vector in 5D.
          * @param u the w-coordinate
          * @param v the u-coordinate
          * @param x the x-coordinate
          * @param y the y-coordinate
          * @param z the z-coordinate
          */
      public:
         Vector(const T& u, const T& v, const T& x, const T& y, const T& z) throws();

         /**
          * Create a vector in 4D.
          * @param u the w-coordinate
          * @param v the u-coordinate
          * @param w the w-coordinate
          * @param x the x-coordinate
          * @param y the y-coordinate
          * @param z the z-coordinate
          */
      public:
         Vector(const T& u, const T& v, const T& w, const T& x, const T& y, const T& z) throws();

         /**
          * Create a new vector in the specified dimension
          * and given values
          * @param dim a dimension
          * @param values the coordinate values of the new vector
          * @pre assertPositive(dim)
          */
      public:
         explicit Vector(size_t dim, const T* values) throws();

         /**
          * Copy-constructs a new Vector object
          * @param source the source Vector object
          */
      public:
         Vector(const Vector& source) throws();

         /**
          * Copy-constructs a new Vector object
          * @param source the source Vector object
          */
      public:
         Vector(Vector&& source) throws();

         /**
          * Creates a new vector whose data buffer is backed by an externally
          * specified array. The lifetime of this vector object must not
          * exceed that of the data buffer. The purpose of this method is to
          * support an idiom, where a raw data buffer is wrapped by vector.
          * @param dim the size of the vector
          * @param data the externally specified data buffer of size dim
          * @pre assert(r!=0 && c!=0 && data!=0)
          */
      public:
         static Vector createWithBuffer(size_t dim, T* data) throws()
         {
            assert(dim != 0 && data != 0);
            Vector v;
            v._dim = dim;
            v._vector = data;
            assert(v._dataBuffer == nullptr);
            return v;
         }

         /**
          * Copies the specified Vector object.
          * @param source the Vector object to be copied
          * @return a reference to this object
          */
      public:
         Vector& operator=(const Vector& source) throws();

         /**
          * Copies the specified Vector object.
          * @param source the Vector object to be copied
          * @return a reference to this object
          */
      public:
         Vector& operator=(Vector&& source) throws();

         /**
          * Destroys this Vector object.
          */
      public:
         inline ~Vector() throws()
         {
            delete[] _dataBuffer;
         }

         /**
          * @name Acessors and Mutators
          * @{
          */

         /**
          * Get the vector dimension.
          * @return the dimension of this vector.
          */
      public:
         inline size_t dimension() const throws()
         {
            return _dim;
         }

         /**
          * Access the specified coordinate.
          * @param i the vector's i^th coordinate
          * @pre REQUIRE_RANGE(i,0,dimension()-1);
          * @return the i^th coordinate value
          */
      public:
         inline T& get(size_t i) throws()
         {
            assert(i < dimension());
            return _vector[i];
         }

         /**
          * Access the specified coordinate.
          * @param i the vector's i^th coordinate
          * @pre REQUIRE_RANGE(i,0,dimension()-1);
          * @return the i^th coordinate value
          */
      public:
         inline const T get(size_t i) const throws()
         {
            assert(i < dimension());
            return _vector[i];
         }

         /**
          * Access the specified coordinate.
          * @param i the vector's i^th coordinate
          * @pre REQUIRE_RANGE(i,0,dimension()-1);
          * @return the i^th coordinate value
          */
      public:
         inline T& operator()(size_t i) throws()
         {
            return get(i);
         }

         /**
          * Access the specified coordinate.
          * @param i the vector's i^th coordinate
          * @pre REQUIRE_RANGE(i,0,dimension()-1);
          * @return the i^th coordinate value
          */
      public:
         inline const T operator()(size_t i) const throws()
         {
            return get(i);
         }

         /**
          * Set the i^th coordinate value.
          * @param i a coordinate index
          * @param value the new coordinate value
          * @pre REQUIRE_RANGE(i,0,dimension()-1);
          */
      private:
         inline void setValue(size_t i, const T& value) throws()
         {
            assert(i < dimension());
            _vector[i] = value;
         }

         /**
          * Set the ^th coordinate.
          * @param i a coordinate index
          * @param value the new coordinate value
          * @pre REQUIRE_RANGE(i,0,dimension()-1);
          */
      public:
         inline void set(size_t i, const T& value) throws()
         {
            setValue(i, value);
         }

         /*@}*/

         /**
          * @name Vector operations
          * @{
          */

         /**
          * Compute the square of the length of this vector.
          * @return the square of the length of this vector
          */
      public:
         T sqrLength() const throws();

         /**
          * Compute the length of this vector.
          * @return this vector's length
          */
      public:
         inline T length() const throws()
         {
            return static_cast< T>(::std::sqrt(sqrLength()));
         }

         /**
          * Compute the square of the distance to the specified vector.
          * @param v a vector
          * @return sqrLength (v-this)
          */
      public:
         T sqrDistance(const Vector& v) const throws();

         /**
          * Negate this vector.
          * Computes the vector (*this) = -(*this)
          */
      public:
         void negate() throws();

         /**
          * Make all entries in this vector non-negative.
          */
      public:
         Vector abs() const throws();

         /**
          * Compute the minimum of the values in this vector.
          * @return minimum value
          */
      public:
         T min() const throws();

         /**
          * Compute the maximum of the values in this vector.
          * @return maximum value
          */
      public:
         T max() const throws();

         /**
          * Compute the inner product of this vector
          * with the specified vector.
          * Computes the vector (*this) * v
          * @param v another vector
          * @return the value (*this) * v
          */
      public:
         T mul(const Vector& v) const throws();

         /**
          * Scale this vector
          * @param s a scalar
          */
      public:
         void scale(const T& s) throws();

         /**
          * Scale this vector by the inverse of a scalar.
          * This method is equivalent to scale(1/s), but
          * the result may be more accurate in terms of
          * roundoff errors.
          * @param s a scalar
          */
      public:
         void invScale(const T& s) throws();

         /**
          * Add the specified vector to this vector.
          * Computes the vector (*this) = (*this) + v.
          * @param v another vector
          */
      public:
         void add(const Vector& v) throws();

         /**
          * Add the specified vector from this vector.
          * Computes the vector (*this) = (*this) - v.
          * @param v another vector
          */
      public:
         void sub(const Vector& v) throws();

         /**
          * Add a multiple of the specified vector to this vector.
          * Computes the vector (*this) = (*this) + s*v.
          * @param s a scalar value
          * @param v another vector
          */
      public:
         void add(const T& s, const Vector& v) throws();

         /**
          * Subtract a multiple of the specified vector from this vector.
          * Computes the vector (*this) = (*this) - s*v.
          * @param s a scalar value
          * @param v another vector
          */
      public:
         void sub(const T& s, const Vector& v) throws();

         /*@}*/

         /**
          * Swap this vector with another.
          * @param m a vector
          */
      public:
         inline void swap(Vector& m) throws()
         {
            ::std::swap(_vector, m._vector);
            ::std::swap(_dataBuffer, m._dataBuffer);
            ::std::swap(_dim, m._dim);
         }

         /**
          * @name Comparison functions
          * @{
          */

         /**
          * Compare two vectors by value.
          * @param v a vector
          * @return true if the vectors are equal by value
          */
      public:
         bool equals(const Vector& v) const throws();

         /**
          * Compare this vector to null
          * @return true if this vector equals 0
          */
      public:
         inline bool operator==(nullptr_t ptr) const throws()
         {
            return _vector == ptr;
         }

         /**
          * Compare this vector to null
          * @return true if this vector is not 0
          */
      public:
         inline bool operator!=(nullptr_t ptr) const throws()
         {
            return _vector != ptr;
         }

         /*@}*/

         /**
          * Init a vector with the specified dimension.
          * @param dim a dimension
          */
      private:
         inline void init(size_t dim) throws()
         {
            assert(dim > 0);
            _dataBuffer = _vector = new T[dim];
            _dim = dim;
         }

         /** The vector implementation */
      private:
         T* _vector;

         /** The data buffer */
      private:
         T* _dataBuffer;

         /** The dimension */
      private:
         size_t _dim;
   };
}

template<class T>
inline bool operator==(const ::newton::Vector< T>& p, const ::newton::Vector< T>& q) throws()
{
   return p.equals(q);
}

template<class T>
inline bool operator!=(const ::newton::Vector< T>& p, const ::newton::Vector< T>& q) throws()
{
   return !p.equals(q);
}

template<class T>
::std::ostream& operator<<(::std::ostream& out, const ::newton::Vector< T>& v) throws();

template<class T>
inline ::newton::Vector< T> operator+(const ::newton::Vector< T>& u, const ::newton::Vector< T>& v) throws()
{
   ::newton::Vector< T> t(u);
   t.add(v);
   return t;
}

template<class T>
inline ::newton::Vector< T> operator-(const ::newton::Vector< T>& u, const ::newton::Vector< T>& v) throws()
{
   ::newton::Vector< T> t(u);
   t.sub(v);
   return t;
}

template<class T>
inline ::newton::Vector< T> operator-(const ::newton::Vector< T>& u) throws()
{
   ::newton::Vector< T> t(u);
   t.negate();
   return t;
}

template<class T>
inline ::newton::Vector< T>& operator+=(::newton::Vector< T>& u, const ::newton::Vector< T>& v) throws()
{
   u.add(v);
   return u;
}

template<class T>
inline ::newton::Vector< T>& operator-=(::newton::Vector< T>& u, const ::newton::Vector< T>& v) throws()
{
   u.sub(v);
   return u;
}

template<class T>
inline T operator*(const ::newton::Vector< T>& u, const ::newton::Vector< T>& v) throws()
{
   return u.mul(v);
}

template<class T, class U>
inline ::newton::Vector< T> operator*(const ::newton::Vector< T>& u, const U& s) throws()
{
   ::newton::Vector< T> t(u);
   t.scale(s);
   return t;
}

template<class T, class U>
inline ::newton::Vector< T>& operator*=(::newton::Vector< T>& u, const U& s) throws()
{
   u.scale(s);
   return u;
}

template<class T, class U>
inline ::newton::Vector< T> operator/(const ::newton::Vector< T>& u, const U& s) throws()
{
   ::newton::Vector< T> t(u);
   t.invScale(s);
   return t;
}

template<class T, class U>
inline ::newton::Vector< T>& operator/=(::newton::Vector< T>& u, const U& s) throws()
{
   u.invScale(s);
   return u;
}

template<class T, class U>
inline ::newton::Vector< T> operator*(const U& s, const ::newton::Vector< T>& u) throws()
{
   ::newton::Vector< T> t(u);
   t.scale(s);
   return t;
}

#endif

#ifndef _NEWTON_VECTOR_TPL
#include <newton/Vector.tpl>
#endif
