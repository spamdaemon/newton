#ifndef _NEWTON_CHOLESKY_H
#define _NEWTON_CHOLESKY_H

#ifndef _NEWTON_MATRIX_H
#include <newton/Matrix.h>
#endif

namespace newton {

   /**
    * This class decomposes a symmetric positive definite matrix into
    * a lower and upper triangulation matrices which are transposes of
    * each other. The Cholesky decomposition can be computed more efficiently
    * than the standard LU decomposition. The matrix can also be used to
    * solve a system of linear equations involving a symmetric, positive definite
    * matrix.
    */
   template<class T = double> class Cholesky
   {
         /**
          * A default constructor
          */
      public:
         inline Cholesky()
         {
         }

         /**
          * Copy constructor.
          * @param src a source object
          */
      public:
         inline Cholesky(const Cholesky& src) = default;

         /**
          * Move constructor.
          * @param src a source object
          */
      public:
         inline Cholesky(Cholesky&& src)throws()
               : _matrix(::std::move(src._matrix))
         {
         }

         /**
          * Copy constructor.
          * @param src a source object
          */
      public:
         Cholesky& operator=(const Cholesky& src) throws()
         {
            _matrix = src._matrix;
            return *this;
         }

         /**
          * Copy constructor.
          * @param src a source object
          */
      public:
         Cholesky& operator=(Cholesky&& src) throws()
         {
            _matrix = ::std::move(src._matrix);
            return *this;
         }

         /**
          * The default constructor.
          * @param sz the size of the matrix
          */
      private:
         Cholesky(size_t n)
               : _matrix(n, n)
         {
         }

         /**
          * Create a Cholesky decomposition of the specified matrix.
          * @param A a symmetric, positive-definite matrix
          * @throws ::std::exception if the matrix is not symmetric, positive-definite
          */
      public:
         Cholesky(const Matrix< T>& A) throws (::std::exception)
               : _matrix(A.rowSize(), A.colSize())
         {
            if (!A.isSquare()) {
               throw ::std::invalid_argument("Not a square matrix");
            }

            const size_t nr = A.rowSize();
            const size_t nc = A.colSize();
            for (size_t k = 0; k < nc; ++k) {
               // compute the diagonal entry
               {
                  T tmp = A(k, k);
                  for (size_t j = 0; j < k; ++j) {
                     const double& l_kj = _matrix(k, j);
                     tmp -= l_kj * l_kj;
                  }
                  if (tmp <= 0.0) {
                     throw ::std::invalid_argument("Matrix is not positive-definite");
                  }
                  tmp = ::std::sqrt(tmp);
                  _matrix(k, k) = tmp;
               }

               // compute the lower-diagonal entries
               for (size_t i = k + 1; i < nr; ++i) {
                  T tmp = A(i, k);
                  if (A(k, i) != tmp) {
                     throw ::std::invalid_argument("Matrix is not symmetric");
                  }
                  for (size_t j = 0; j < k; ++j) {
                     tmp -= _matrix(i, j) * _matrix(k, j);
                  }
                  tmp /= _matrix(k, k);
                  _matrix(i, k) = tmp;
               }
            }
         }

         /** The destructor */
      public:
         ~Cholesky() throws()
         {
         }

         /**
          * Check that the specified matrix is indeed lower-triangular
          * with a non-negative diagonal
          * @param cholesky a matrix
          * @param strict if true then diagonal entries must be >0
          * @return true if cholesky is a lower-triangular matrix
          */
      public:
         static bool isCholesky(const Matrix< T>& cholesky, bool strict = true) throws()
         {
            if (!cholesky.isSquare()) {
               return false;
            }
            for (size_t i = 0, sz = cholesky.rowSize(); i < sz; ++i) {
               double tmp = cholesky(i, i);
               if (tmp < 0 || (tmp == 0 && strict)) {
                  return false;
               }

               for (size_t j = 0; j < i; ++j) {
                  if (cholesky(j, i) != 0) { // upper triangular matrix is not 0
                     return false;
                  }
               }
            }
            return true;
         }

         /**
          * Assign the lower-triangular matrix to this cholesky decomposition.
          * @param cholesky a matrix that is the result of a cholesky decomposition
          * @pre isCholesky(cholesky)
          */
      public:
         void assign(const Matrix< T>& cholesky) throws (::std::exception)
         {
            assert(isCholesky(cholesky));
            _matrix = cholesky;
         }

         /**
          * Swap the cholesky matrix with the specified matrix. This method
          * may be implemented by calling the swap method on the cholesky
          * and therefore be more efficient than using assignment.
          *
          * @param cholesky a matrix that is the result of a cholesky decomposition
          * @pre isCholesky(cholesky)
          */
      public:
         void swap(Matrix< T>& cholesky) throws (::std::exception)
         {
            assert(isCholesky(cholesky));
            _matrix.swap(cholesky);
         }

         /**
          * Get the lower triangular matrix L of the decomposition.
          * @return the lower-triangular matrix L
          */
      public:
         inline const Matrix< T>& matrix() const throws()
         {
            return _matrix;
         }

         /**
          * Get the original matrix. This method performs a matrix multiply
          * to reconstitute the original matrix. The result will not exactly
          * be the same as the original matrix due to round-off errors.
          * @return matrix().multiplyTranspose(matrix())
          */
      public:
         Matrix< T> originalMatrix() const throws()
         {
            return _matrix.multiplyTranspose(_matrix);
         }

         /**
          * Get the determinant of the lower triangular matrix. The determinant
          * of the original matrix A is the square of this determinant. If this
          * method returns 0, then the cholesky has not been performed.
          * @return the determinant of matrix()
          */
      public:
         T determinant() const throws()
         {
            size_t sz = _matrix.rowSize();
            if (sz == 0) {
               return 0;
            }
            double det = _matrix(0, 0);
            for (size_t i = 1; i < sz; ++i) {
               det *= _matrix(i, i);
            }
            return det;
         }

         /**
          * Create a Cholesky decomposition that is the inverse
          * @return the inverse cholesky
          */
      public:
         Cholesky inverse() const throws()
         {
            size_t n = _matrix.rowSize();
            Cholesky inv(n);
            // do forward substitution k times
            for (size_t k = 0; k < n; ++k) {
               for (size_t i = k; i < n; ++i) {
                  T tmp = (i == k ? 1 : 0);
                  for (size_t j = 0; j < i; ++j) {
                     tmp -= _matrix(i, j) * inv._matrix(j, k);
                  }
                  tmp /= _matrix(i, i);
                  inv._matrix(i, k) = tmp;
               }
            }
            return inv;
         }

         /**
          * @name Solving systems of equations.
          * To solve a system of equations of the form Ax=b, use Cholesky decomposition as follows:
          * @code
          *   Cholesky ch(A);
          *   Vector y,x;
          *   ch.doForwardSubstitution(b,y);
          *   ch.doBackwardSubstitution(y,x);
          * @endcode
          * Using both forward and backward substitution, one can multiply
          * a vector (or matrix) by the inverse of the lower or upper triangular matrix, respectively.
          * Using this, one can quickly calculate the following:
          * @code d = x*A.inverse().x@endcode
          *  using backward substitution
          * @code
          *   Cholesky ch(A);
          *   Vector tmp;
          *   ch.doBackwardSubstitution(x,tmp);
          *   d= tmp*tmp;
          * @endcode
          * or forward substitution
          * @code
          *   Cholesky ch(A);
          *   Vector tmp;
          *   ch.doForwardSubstitution(x,tmp);
          *   d= tmp*tmp;
          * @endcode
          */

         /**
          * Perform the forward substitution algorithm on a vector. Forward substitution
          * is equivalent to matrix().inverse()*v.
          * @note it is explicitly allowed to have &u == &v.
          * @param v a vector
          * @param u the result vector
          */
      public:
         void doForwardSubstitution(const Vector< T>& v, Vector< T>& u) const throws (::std::exception)
         {
            size_t sz = v.dimension();
            if (sz != _matrix.rowSize()) {
               throw ::std::invalid_argument("Invalid vector dimension");
            }
            u(0) = v(0) / _matrix(0, 0);

            for (size_t i = 1; i < sz; ++i) {
               T tmp = v(i);
               for (size_t j = 0; j < i; ++j) {
                  tmp -= _matrix(i, j) * u(j);
               }
               tmp /= _matrix(i, i);
               u(i) = tmp;
            }
         }

         /**
          * Perform the forward substitution algorithm on a matrix. This
          * function is equivalent to matrix().inverse()*v.
          * @note it is explicitly allowed to have &u == &v.
          * @param v a vector
          * @param u the result vector
          */
      public:
         void doForwardSubstitution(const Matrix< T>& v, Matrix< T>& u) const throws (::std::exception)
         {
            size_t sz = v.rowSize();
            if (sz != _matrix.rowSize()) {
               throw ::std::invalid_argument("Invalid vector dimension");
            }
            if (v.colSize() != u.colSize()) {
               throw ::std::invalid_argument("Incompatible matrix column sizes");
            }
            for (size_t k = 0; k < v.colSize(); ++k) {
               u(0, k) = v(0, k) / _matrix(0, 0);
               for (size_t i = 1; i < sz; ++i) {
                  T tmp = v(i, k);
                  for (size_t j = 0; j < i; ++j) {
                     tmp -= _matrix(i, j) * u(j, k);
                  }
                  tmp /= _matrix(i, i);
                  u(i, k) = tmp;
               }
            }
         }

         /**
          * Perform the backward substitution algorith on the transpose of the Cholesky matrix.
          * Backward substitution is equivalent to v*matrix().transpose().inverse().
          * @note it is explicitly allowed to have &u == &v.
          * @param v a vector
          * @param u the result vector
          */
      public:
         void doBackwardSubstitution(const Vector< T>& v, Vector< T>& u) const throws (::std::exception)
         {
            size_t sz = v.dimension();
            if (sz != _matrix.rowSize()) {
               throw ::std::invalid_argument("Invalid vector dimension");
            }
            size_t i = sz - 1;
            u(i) = v(i) / _matrix(i, i);

            while (i-- > 0) {
               T tmp = v(i);
               for (size_t j = i + 1; j < sz; ++j) {
                  tmp -= u(j) * _matrix(j, i); // remember transposed matrix!
               }
               tmp /= _matrix(i, i);
               u(i) = tmp;
            }
         }

         /**
          * Perform the backward substitution algorith on the transpose of the Cholesky matrix.
          * @note it is explicitly allowed to have &u == &v.
          * @param v a matrix
          * @param u the result matrix
          */
      public:
         void doBackwardSubstitution(const Matrix< T>& v, Matrix< T>& u) const throws (::std::exception)
         {
            size_t sz = v.rowSize();
            if (sz != _matrix.rowSize()) {
               throw ::std::invalid_argument("Invalid vector dimension");
            }
            if (v.colSize() != u.colSize()) {
               throw ::std::invalid_argument("Incompatible matrix column sizes");
            }

            for (size_t k = 0; k < v.colSize(); ++k) {
               size_t i = sz - 1;
               u(i, k) = v(i, k) / _matrix(i, i);

               while (i-- > 0) {
                  T tmp = v(i, k);
                  for (size_t j = i + 1; j < sz; ++j) {
                     tmp -= u(j, k) * _matrix(j, i); // remember transposed matrix!
                  }
                  tmp /= _matrix(i, i);
                  u(i, k) = tmp;
               }
            }
         }

         /**
          * Pre-multiply the lower-triangular matrix by v.
          * @param v a vector
          * @return a new vector
          */
      public:
         Vector< T> preMultiply(const Vector< T>& v) const throws ( ::std::exception)
         {
            size_t sz = v.dimension();
            Vector< T> u(sz);
            for (size_t i = 0; i < sz; ++i) {
               T tmp = 0;
               for (size_t j = i; j < sz; ++j) {
                  tmp += v(j) * _matrix(j, i);
               }
               u(i) = tmp;
            }
            return u;
         }

         /**
          * Compute the inner product of the vector matrix().transpose()*v.
          * This calculation is fairly efficient and requires no extra storage.
          * @param v a vector
          * @return a scalar
          */
      public:
         T innerProduct(const Vector< T>& v) const throws ( ::std::exception)
         {
            size_t sz = v.dimension();
            T ip = 0;
            for (size_t i = 0; i < sz; ++i) {
               T tmp = 0;
               for (size_t j = i; j < sz; ++j) {
                  tmp += v(j) * _matrix(j, i);
               }
               ip += tmp * tmp;
            }
            return ip;
         }

         /**
          * Swap this Cholesky with another.
          * @param ch a cholesky
          */
      public:
         inline void swap(Cholesky& ch) throws()
         {
            _matrix.swap(ch._matrix);
         }

         /** The lower-triangular matrix  */
      private:
         Matrix< T> _matrix;
   };

}

#endif
