#ifndef _NEWTON_VECTOR_H
#include <newton/Vector.h>
#endif

#ifndef _NEWTON_VECTOR_TPL
#define _NEWTON_VECTOR_TPL

#ifndef _CANOPY_ARRAYCOPY_H
#include <canopy/arraycopy.h>
#endif

#include <iostream>
#include <algorithm>
#include <cmath>

namespace newton {

   template <class T>
   Vector<T>::Vector (size_t dim) throws()
   {
      if (dim>0) {
         init(dim);
         ::std::fill(_vector,_vector+dim,0.0);
      }

      else {
         _dim = 0;
         _vector = 0;
      }
   }

   template <class T>
   Vector<T>::Vector(Vector&& source) throws()
   :_vector(source._vector),
   _dataBuffer(source._dataBuffer), _dim(source._dim)
   {
      source._vector = nullptr;
      source._dataBuffer = nullptr;
      source._dim = 0;
   }

   template <class T>
   Vector<T> Vector<T>::create(size_t dim, const T& initial) throws()
   {
      Vector res;
      if (dim>0) {
         res.init(dim);
         ::std::fill(res._vector,res._vector+dim,initial);
      }
      return res;
   }

   template <class T>
   Vector<T>::Vector (const T& x, const T& y) throws()
   {
      init(2);
      setValue(0,x);
      setValue(1,y);
   }

   template <class T>
   Vector<T>::Vector (const T& x, const T& y, const T& z) throws()
   {
      init(3);
      setValue(0,x);
      setValue(1,y);
      setValue(2,z);
   }

   template <class T>
   Vector<T>::Vector (const T& u, const T& x, const T& y, const T& z) throws()
   {
      init(4);
      setValue(0,u);
      setValue(1,x);
      setValue(2,y);
      setValue(3,z);
   }

   template <class T>
   Vector<T>::Vector (const T& u, const T& v, const T& x, const T& y, const T& z) throws()
   {
      init(5);
      setValue(0,u);
      setValue(1,v);
      setValue(2,x);
      setValue(3,y);
      setValue(4,z);
   }

   template <class T>
   Vector<T>::Vector (const T& u, const T& v, const T& w, const T& x, const T& y, const T& z) throws()
   {
      init(6);
      setValue(0,u);
      setValue(1,v);
      setValue(2,w);
      setValue(3,x);
      setValue(4,y);
      setValue(5,z);
   }

   template <class T>
   Vector<T>::Vector(size_t dim, const T* values) throws()
   {
      if (dim==0) {
         _dim = 0;
         _vector = 0;
      }
      else {
         init(dim);
         ::canopy::arraycopy(values,_vector,_dim);
      }
   }

   template <class T>
   Vector<T>::Vector(const Vector& source) throws()
   {
      if (source._vector!=0) {
         init(source._dim);
         ::canopy::arraycopy(source._vector,_vector,_dim);
      }
      else {
         _vector=nullptr;
         _dim=0;
      }
   }

   template <class T>
   Vector<T>& Vector<T>::operator= (const Vector& source) throws()
   {
      if (_vector!=source._vector) {
         if (_dim!=source._dim) {
            if (_vector!=nullptr) {
               delete [] _dataBuffer;
               _dataBuffer = _vector = nullptr;
               _dim = 0;
            }
            if (source._vector!=0) {
               _dataBuffer = _vector = new T[source._dim];
               _dim = source._dim;
            }
         }
         ::canopy::arraycopy(source._vector,_vector,_dim);
      }
      return *this;
   }

   template <class T>
   Vector<T>& Vector<T>::operator=(Vector&& source) throws()
   {
      if (this != &source) {
         delete[] _dataBuffer;
         _vector = source._vector;
         _dataBuffer = source._dataBuffer;
         _dim = source._dim;
         source._vector = nullptr;
         source._dataBuffer = nullptr;
         source._dim = 0;
      }
      return *this;
   }

   template <class T>
   T Vector<T>::sqrLength () const throws()
   {
      T len=0;
      for (const T* i=_vector;i<_vector+_dim;++i) {
         len += (*i) * (*i);
      }
      return len;
   }

   template <class T>
   bool Vector<T>::equals (const Vector& v) const throws()
   {
      if (_vector!=v._vector) {
         if (v.dimension()!=_dim) {
            return false;
         }
         for (const T* i=_vector,*j=v._vector;i<_vector+_dim;++i,++j) {
            if (*i != *j) {
               return false;
            }
         }
      }
      return true;
   }

   template <class T>
   T Vector<T>::mul (const Vector& v) const throws()
   {
      assert(dimension()==v.dimension());
      T ip(0);
      for (const T* i=_vector,*j=v._vector;i<_vector+_dim;++i,++j) {
         ip += (*i) * (*j);
      }
      return ip;
   }

   template <class T>
   T Vector<T>::min() const throws()
   {
      assert(_dim>0);
      return *::std::min_element(_vector,_vector+_dim);
   }

   template <class T>
   T Vector<T>::max() const throws()
   {
      assert(_dim>0);
      return *::std::max_element(_vector,_vector+_dim);
   }

   template <class T>
   Vector<T> Vector<T>::abs() const throws()
   {
      Vector<T> res(*this);
      for (T* i=res._vector;i<res._vector+res._dim;++i) {
         *i = ::std::abs(*i);
      }
      return res;
   }

   template <class T>
   void Vector<T>::negate () throws()
   {
      for (T* i=_vector;i<_vector+_dim;++i) {
         *i = -(*i);
      }
   }

   template <class T>
   void Vector<T>::scale (const T& s) throws()
   {
      for (T* i=_vector;i<_vector+_dim;++i) {
         *i *= s;
      }
   }

   template <class T>
   void Vector<T>::invScale (const T& s) throws()
   {
      for (T* i=_vector;i<_vector+_dim;++i) {
         *i /= s;
      }
   }

   template <class T>
   void Vector<T>::add (const Vector& v) throws()
   {
      assert(dimension()==v.dimension());
      for (T* i=_vector,*j=v._vector;i<_vector+_dim;++i,++j) {
         (*i) += (*j);
      }
   }

   template <class T>
   void Vector<T>::sub (const Vector& v) throws()
   {
      assert(dimension()==v.dimension());
      for (T* i=_vector,*j=v._vector;i<_vector+_dim;++i,++j) {
         (*i) -= (*j);
      }
   }

   template <class T>
   void Vector<T>::add (const T& s, const Vector& v) throws()
   {
      assert(dimension()==v.dimension());
      for (T* i=_vector,*j=v._vector;i<_vector+_dim;++i,++j) {
         (*i) += s*(*j);
      }
   }

   template <class T>
   void Vector<T>::sub (const T& s, const Vector& v) throws()
   {
      assert(dimension()==v.dimension());
      for (T* i=_vector,*j=v._vector;i<_vector+_dim;++i,++j) {
         (*i) -= s*(*j);
      }
   }

   template <class T>
   T Vector<T>::sqrDistance (const Vector& v) const throws()
   {
      assert(dimension()==v.dimension());
      T dist(0);
      for (const T* i=_vector,*j=v._vector;i<_vector+_dim;++i,++j) {
         double tmp = (*i) - (*j);
         dist += tmp*tmp;
      }

      return dist;
   }
}

template <class T>
::std::ostream& operator<< (::std::ostream& out, const ::newton::Vector<T>& v) throws()
{
   out << '[';
   for (size_t i=0, sz=v.dimension();i<sz;++i) {
      out << ' '<< v(i);
   }
   out << " ]";
   return out;
}

#endif
