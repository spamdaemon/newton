#ifndef _NEWTON_MATRIX_H
#define _NEWTON_MATRIX_H

#ifndef _NEWTON_H
#include <newton/newton.h>
#endif

#ifndef _NEWTON_VECTOR_H
#include <newton/Vector.h>
#endif

#include <cassert>

namespace newton {
   /**
    * This is a matrix class. It provides various
    * matrix operations. Instances of this class
    * are templated on the type of the values stored
    * in the matrix.
    */
   template<class T = double> class Matrix
   {

         /**
          * Creates a new Matrix instance.
          */
      public:
         inline Matrix() throws()
               : _rowSize(0), _colSize(0), _dataBuffer(nullptr), _data(nullptr)
         {
         }

         /**
          * Creates a new Matrix instance.
          */
      public:
         inline Matrix(::std::nullptr_t) throws()
               : _rowSize(0), _colSize(0), _dataBuffer(nullptr), _data(nullptr)
         {
         }

         /**
          * Creates a new square Matrix instance.
          * @param sz the size of the matrix
          */
      public:
         inline Matrix(size_t sz) throws()
         {
            if (sz == 0) {
               _rowSize = _colSize = 0;
               _dataBuffer = _data = nullptr;
            }
            else {
               initialize(sz, sz);
            }
         }

         /**
          * Creates a new Matrix instance from the outer product of two vectors.
          * @param u a vector
          * @param v a vector
          */
      public:
         inline Matrix(const Vector< T>& u, const Vector< T>& v) throws()
               : _rowSize(u.dimension()), _colSize(v.dimension()), _dataBuffer(0), _data(0)
         {
            _dataBuffer = _data = new T[_rowSize * _colSize];
            for (size_t i = 0; i < u.dimension(); ++i) {
               for (size_t j = 0; j < v.dimension(); ++j) {
                  _data[calcOffset(i, j)] = u(i) * v(j);
               }
            }
         }

         /**
          * Creates a new square Matrix instance.
          * @param r the number or rows
          * @param c the number of columns
          * @pre assert((r>0 && c>0) || (r==0 && c==0))
          */
      public:
         Matrix(size_t r, size_t c) throws()
         {
            initialize(r, c, 0);
         }

         /**
          * Creates a new square Matrix instance.
          * @param r the number or rows
          * @param c the number of columns
          * @param defaultValue the default value for each entry
          * @pre assert((r>0 && c>0) || (r==0 && c==0))
          */
      public:
         static Matrix create(size_t r, size_t c, const T& defaultValue) throws()
         {
            Matrix m;
            if (r != 0 && c != 0) {
               size_t sz = r * c;
               m._rowSize = r;
               m._colSize = c;
               m._dataBuffer = m._data = new T[sz];
               ::std::fill(m._data, m._data + sz, defaultValue);
            }
            return m;
         }

         /**
          * Creates a new matrix whose data buffer is backed by an externally
          * specified array. The lifetime of this matrix object must not
          * exceed that of the data buffer.
          * @param r the number or rows
          * @param c the number of columns
          * @param data the externally specified data buffer
          * @pre assert(r!=0 && c!=0 && data!=0)
          */
      public:
         static Matrix createWithBuffer(size_t r, size_t c, T* data) throws()
         {
            assert(r != 0 && c != 0 && data != nullptr);
            Matrix m;
            m._rowSize = r;
            m._colSize = c;
            // not setting the databuffer as it's initialized to 0 anyway
            assert(m._dataBuffer == 0);
            m._data = data;
            return m;
         }

         /**
          * Copy-constructs a new Matrix object
          * @param source the source Matrix object
          */
      public:
         inline Matrix(const Matrix& source) throws()
         {
            initialize(source._rowSize, source._colSize, &source);
         }

         /**
          * Move constructor.
          * @param source the source Matrix object
          */
      public:
         Matrix(Matrix&& source) throws();

         /**
          * Copies the specified Matrix object.
          * @param source the Matrix object to be copied
          * @return a reference to this object
          */
      public:
         Matrix& operator=(const Matrix& source) throws();

         /**
          * Copies the specified Matrix object.
          * @param source the Matrix object to be copied
          * @return a reference to this object
          */
      public:
         Matrix& operator=(Matrix&& source) throws();

         /**
          * Destroys this Matrix object.
          */
      public:
         inline ~Matrix() throws()
         {
            delete[] _dataBuffer;
         }

         /**
          * Create the identiy matrix.
          * @param n the matrix must be square
          * @param defaultValue the value along the diagonal
          * @return the identity matrix of dimension n
          */
      public:
         static Matrix identity(size_t n, const T& defaultValue = 1.0) throws()
         {
            Matrix m(n);
            for (size_t i = 0; i < n; ++i) {
               m(i, i) = defaultValue;
            }
            return m;
         }

         /**
          * Create the identiy matrix whose diagonal entries are
          * defined by a vector.
          * @param diag a vector of diagonal entries
          * @return the diagonal
          */
      public:
         static Matrix diagonal(const Vector< T>& diag) throws()
         {
            Matrix m(diag.dimension());
            for (size_t i = 0; i < diag.dimension(); ++i) {
               m(i, i) = diag(i);
            }
            return m;
         }

         /**
          * @name Accessor for various matrix properties
          * @{*/
         /**
          * Get the row dimension of this matrix
          * @return the number of rows
          */
      public:
         inline size_t rowSize() const throws()
         {
            return _rowSize;
         }

         /**
          * Get the column dimension of this matrix
          * @return the number of columns
          */
      public:
         inline size_t colSize() const throws()
         {
            return _colSize;
         }

         /**
          * Compare two matrices
          * @param m a matrix of  the same dimensionality
          * @return true if the matrices are the same
          */
      public:
         bool equals(const Matrix& m) const throws();

         /**
          * Test if this is a square matrix. A matrix is square
          * if <code>colSize()==rowSize() && colSize()>0</code>
          * @return true if this matrix is a square matrix.
          */
      public:
         inline bool isSquare() const throws()
         {
            return _colSize > 0 && _colSize == _rowSize;
         }

         /**
          * Access the specified element. The values are
          * accessed as
          * <pre>
          *   A11...A1N.
          *   A21...A2N
          *   AM1...AMN
          * </pre> which is more inline with mathematic algoritms.
          * @param row a row
          * @param col a column
          * @pre REQUIRE_RANGE(row,0,_rowSize-1);
          * @pre REQUIRE_RANGE(row,0,_rowSize-1);
          * @return the value at (row,col)
          */
      public:
         inline const T& get(size_t row, size_t col) const throws()
         {
            return _data[calcOffset(row, col)];
         }

         /**
          * Access the specified element.
          * @param row a row
          * @param col a column
          * @pre REQUIRE_RANGE(row,0,_rowSize-1);
          * @pre REQUIRE_RANGE(row,0,_rowSize-1);
          * @return the value at (row,col)
          */
      public:
         inline T& get(size_t row, size_t col) throws()
         {
            return _data[calcOffset(row, col)];
         }

         /**
          * Access the specified element. The values are
          * accessed as
          * <pre>
          *   A11...A1N.
          *   A21...A2N
          *   AM1...AMN
          * </pre> which is more inline with mathematic algoritms.
          * @param row a row
          * @param col a column
          * @pre REQUIRE_RANGE(row,0,_rowSize-1);
          * @pre REQUIRE_RANGE(row,0,_rowSize-1);
          * @return the value at (row,col)
          */
      public:
         inline const T& operator()(size_t row, size_t col) const throws()
         {
            return get(row, col);
         }

         /**
          * Access the specified element.
          * @param row a row
          * @param col a column
          * @pre REQUIRE_RANGE(row,0,_rowSize-1);
          * @pre REQUIRE_RANGE(row,0,_rowSize-1);
          * @return the value at (row,col)
          */
      public:
         inline T& operator()(size_t row, size_t col) throws()
         {
            return get(row, col);
         }

         /*@}*/

         /**
          * @name Object validity tests
          * @{
          */
         /**
          * Test if this matrix has no rows and no columns. This method can be
          * used to test if method can be invoked on this object.  Usually
          * this method is used like <code>if(m==0) { }</code>.
          * @param m some pointer.
          * @pre m==0
          * @return true if m ==0 and the size of the matrix is 0, false otherwise
          */
      public:
         inline bool operator==(::std::nullptr_t m) const throws()
         {
            return _data == m;
         }

         /**
          * Test if this matrix has rows and columns. This method can be
          * used to test if method can be invoked on this object. Usually
          * this method is used like <code>if(m!=0) { }</code>.
          * @param m some pointer.
          * @pre m==0
          * @return true if m ==0 and the size of the matrix is not 0, false otherwise
          */
      public:
         inline bool operator!=(::std::nullptr_t m) const throws()
         {
            return _data != m;
         }

         /*@}*/

         /**
          * Get the determinant of this matrix.
          * @return the determinant of this matrix
          */
      public:
         T determinant() const throws();

         /**
          * @name Vector-matrix calculations
          * @{
          */

         /**
          * Add the outer product of two vectors to this matrix.
          * @param u a vector
          * @param v a vector
          * @pre u.dimension()==rowSize()
          * @pre v.dimension()==colSize()
          * @return this matrix.
          */
      public:
         Matrix< T>& addOuterProduct(const Vector< T>& u, const Vector< T>& v) throws()
         {
            assert(u.dimension() == rowSize());
            assert(v.dimension() == colSize());

            for (size_t i = 0; i < u.dimension(); ++i) {
               for (size_t j = 0; j < v.dimension(); ++j) {
                  _data[calcOffset(i, j)] = u(i) * v(j);
               }
            }
            return *this;
         }

         /**
          * Premultiply the specified vector by this
          * matrix.
          * @param v a vector
          * @pre REQUIRE_EQUAL(v.dimension(),colSize())
          * @return the vector (*this) * v
          */
      public:
         Vector< T> preMultiply(const Vector< T>& v) const throws();

         /**
          * Premultiply the specified vector by this
          * matrix. res = preMultiply(v);
          * @param v a vector
          * @param res the resulting vector
          * @pre REQUIRE_EQUAL(v.dimension(),colSize())
          */
      public:
         void preMultiply(const Vector< T>& v, Vector< T>& res) const throws();

         /**
          * Postmultiply the specified vector by this
          * matrix.
          * @param v a vector
          * @pre REQUIRE_EQUAL(v.dimension(),rowSize())
          * @return the vector v * (*this)
          */
      public:
         Vector< T> postMultiply(const Vector< T>& v) const throws();

         /**
          * Postmultiply the specified vector by this
          * matrix.
          * @param v a vector
          * @param res the result vector
          * @pre REQUIRE_EQUAL(v.dimension(),rowSize())
          */
      public:
         void postMultiply(const Vector< T>& v, Vector< T>& res) const throws();

         /**
          * Multiply this matrix by another matrix and place the result in
          * the result argument. The result may not be *this or m
          *
          * @param m another matrix
          * @param result the matrix (*this) * m
          * @pre (&result != this) && (&m != &result);
          * @pre REQUIRE_EQUAL(colSize(),m.rowSize())
          */
      public:
         void multiply(const Matrix< T>& m, Matrix< T>& result) const throws();

         /**
          * Multiply this matrix by another matrix and place the result in
          * the result argument. The result may not be *this or m
          *
          * @param m another matrix
          * @pre (&result != this) && (&m != &result);
          * @pre REQUIRE_EQUAL(colSize(),m.rowSize())
          * @return the matrix (*this) * m
          */
      public:
         Matrix< T> multiply(const Matrix< T>& m) const throws()
         {
            Matrix< T> res(_rowSize, m._colSize);
            multiply(m, res);
            return res;
         }

         /**
          * Multiply this matrix by the transpose of another matrix.
          * @param m another matrix
          * @param result the matrix (*this) * m
          * @pre (&result != this) && (&m != &result);
          * @pre REQUIRE_EQUAL(colSize(),m.rowSize())
          */
      public:
         void multiplyTranspose(const Matrix< T>& m, Matrix< T>& result) const throws();

         /**
          * Multiply this matrix by the transpose of another matrix.
          * @param m another matrix
          * @pre REQUIRE_EQUAL(colSize(),m.rowSize())
          * @return the matrix (*this) * m.transpose()
          */
      public:
         Matrix< T> multiplyTranspose(const Matrix< T>& m) const throws()
         {
            Matrix< T> res(_rowSize, m._rowSize);
            multiplyTranspose(m, res);
            return res;
         }

         /**
          * Compute the minimum of all entries in this matrix
          * @return minimum value of all entries in this matrix
          */
      public:
         T min() const throws();

         /**
          * Compute the maximum of all entries in this matrix
          * @return maximum value of all entries in this matrix
          */
      public:
         T max() const throws();

         /**
          * Make all entries in this matrix non-negative.
          */
      public:
         Matrix abs() const throws();

         /**
          * Negate this matrix.
          * Computes the matrix (*this) = -(*this)
          */
      public:
         void negate() throws();

         /**
          * Transpose this matrix.
          */
      public:
         void transposeMatrix() throws();

         /**
          * Transpose this matrix.
          * @return the transpose of this matrix.
          */
      public:
         Matrix transpose() const throws ();

         /**
          * Invert this matrix. Compute the inverse of  this matrix. If the
          * right matrix is specified, then this method will set this matrix
          * to the following <code>inv(this) * right</code>. This
          * is more efficient than computing the inverse and then applying a multiplication.
          * @param right a matrix that will will be premultiplied by the inverse of this matrix.
          * @return the determinant of the original (non-inverted) matrix
          * @throws ::std::exception if the matrix is not invertible
          */
      public:
         T invert(const Matrix* right = 0) throws (::std::exception);

         /**
          * Determine the inverse of this matrix.
          * @return the inverse of this matrix.
          * @param right a matrix that will will be premultiplied by the inverse of this matrix.
          * @param pdet an optional return value for the the determinant of the original (non-inverted) matrix
          * @throws ::std::exception if the matrix is not invertible
          */
      public:
         Matrix inverse(const Matrix* right = 0, T* pdet = 0) const throws (::std::exception)
         {
            Matrix res(*this);
            T det = res.invert(right);
            if (pdet) {
               *pdet = det;
            }
            return res;
         }

         /**
          * Scale this matrix
          * @param s a scalar
          */
      public:
         void scale(const T& s) throws();

         /**
          * Scale this matrix by the inverse of a scalar.
          * This method is equivalent to scale(1/s), but
          * the result may be more accurate in terms of
          * roundoff errors.
          * @param s a scalar
          */
      public:
         void invScale(const T& s) throws();

         /**
          * Add the specified matrix to this matrix.
          * Computes the matrix (*this) = (*this) + v.
          * @param v another matrix
          */
      public:
         void add(const Matrix& v) throws();

         /**
          * Add the specified matrix from this matrix.
          * Computes the matrix (*this) = (*this) - v.
          * @param v another matrix
          */
      public:
         void sub(const Matrix& v) throws();

         /**
          * Add a multiple of the specified matrix to this matrix.
          * Computes the matrix (*this) = (*this) + s*v.
          * @param s a scalar value
          * @param v another matrix
          */
      public:
         void add(const T& s, const Matrix& v) throws();

         /**
          * Subtract a multiple of the specified matrix from this matrix.
          * Computes the matrix (*this) = (*this) - s*v.
          * @param s a scalar value
          * @param v another matrix
          */
      public:
         void sub(const T& s, const Matrix& v) throws();

         /*@}*/

         /**
          * Determine if this matrix is positive definite.
          * This method uses the <em>Sylvester Condition</em>
          * to determine positive-definiteness. If this matrix
          * is not square, then an illegal argument is thrown
          * @return true if this matrix is positive definite
          * @throws ::std::invalid_argument if this matrix is not square
          */
      public:
         bool isPositiveDefinite() const throws (::std::invalid_argument);

         /**
          * Swap this matrix with another.
          * @param m a matrix
          */
      public:
         void swap(Matrix& m) throws()
         {
            ::std::swap(_rowSize, m._rowSize);
            ::std::swap(_colSize, m._colSize);
            ::std::swap(_dataBuffer, m._dataBuffer);
            ::std::swap(_data, m._data);
         }

         /**
          * Initialize this matrix.
          * @param r the number of rows
          * @param c the number of cols
          * @param data optional data of size rowSize*colSize
          * @parma defaultValue a default value if (optional data is omitted)
          */
      private:
         void initialize(size_t r, size_t c, const Matrix* data = 0) throws();

         /**
          * Calculate the data index for the specified row and column
          * @param row a row index
          * @param col a column index
          * @pre REQUIRE_RANGE(row,0,_rowSize-1);
          * @pre REQUIRE_RANGE(row,0,_rowSize-1);
          * @return an offset in the the data
          */
      private:
         inline size_t calcOffset(size_t row, size_t col) const throws()
         {
            assert(row < _rowSize);
            assert(col < _colSize);
            return row * _colSize + col;
         }

         /**
          * Get a pointer to the specified row.
          * @param r a row number
          * @return a pointer to the row
          */
      private:
         inline const T* getRow(size_t r) const throws()
         {
            return &_data[calcOffset(r, 0)];
         }

         /**
          * Get a pointer to the specified row.
          * @param r a row number
          * @return a pointer to the row
          */
      private:
         inline T* getRow(size_t r) throws()
         {
            return &_data[calcOffset(r, 0)];
         }

         /** The number of rows this matrix */
      private:
         size_t _rowSize;

         /** The number of columns */
      private:
         size_t _colSize;

         /** The data buffer */
      private:
         T* _dataBuffer;

         /** The data */
      private:
         T* _data;
   };
}

template<class T>
::newton::Vector< T> operator*(const ::newton::Matrix< T>& m, const ::newton::Vector< T>& v) throws()
{
   return m.preMultiply(v);
}

template<class T>
::newton::Vector< T> operator*(const ::newton::Vector< T>& v, const ::newton::Matrix< T>& m) throws()
{
   return m.postMultiply(v);
}

template<class T>
::newton::Matrix< T> operator*(const ::newton::Matrix< T>& m, const ::newton::Matrix< T>& n) throws()
{
   return m.multiply(n);
}

template<class T>
inline ::newton::Matrix< T> operator+(const ::newton::Matrix< T>& u, const ::newton::Matrix< T>& v) throws()
{
   ::newton::Matrix< T> t(u);
   t.add(v);
   return t;
}

template<class T>
inline ::newton::Matrix< T> operator-(const ::newton::Matrix< T>& u, const ::newton::Matrix< T>& v) throws()
{
   ::newton::Matrix< T> t(u);
   t.sub(v);
   return t;
}

template<class T>
inline ::newton::Matrix< T> operator-(const ::newton::Matrix< T>& u) throws()
{
   ::newton::Matrix< T> t(u);
   t.negate();
   return t;
}

template<class T>
inline ::newton::Matrix< T>& operator+=(::newton::Matrix< T>& u, const ::newton::Matrix< T>& v) throws()
{
   u.add(v);
   return u;
}

template<class T>
inline ::newton::Matrix< T>& operator-=(::newton::Matrix< T>& u, const ::newton::Matrix< T>& v) throws()
{
   u.sub(v);
   return u;
}

template<class T, class U>
inline ::newton::Matrix< T> operator*(const ::newton::Matrix< T>& u, const U& s) throws()
{
   ::newton::Matrix< T> t(u);
   t.scale(s);
   return t;
}

template<class T, class U>
inline ::newton::Matrix< T>& operator*=(::newton::Matrix< T>& u, const U& s) throws()
{
   u.scale(s);
   return u;
}

template<class T, class U>
inline ::newton::Matrix< T> operator/(const ::newton::Matrix< T>& u, const U& s) throws()
{
   ::newton::Matrix< T> t(u);
   t.invScale(s);
   return t;
}

template<class T, class U>
inline ::newton::Matrix< T>& operator/=(::newton::Matrix< T>& u, const U& s) throws()
{
   u.invScale(s);
   return u;
}

/**
 * Multiply a matrix by the inverse of another matrix.
 * @param s the scalar
 * @param u the matrix to be inverted
 * @return s=s*u.inverse();
 */
template<class T>
inline ::newton::Matrix< T>& operator/=(::newton::Matrix< T>& s,
      const ::newton::Matrix< T>& u) throws (::std::exception)
{
   return s *= u.inverse();
}

/**
 * Invert a matrix and scale it by a value
 * @param s the a matrix
 * @param u the matrix to be inverted
 * @return s*u.inverse()
 */
template<class T>
inline ::newton::Matrix< T> operator/(const ::newton::Matrix< T>& s,
      const ::newton::Matrix< T>& u) throws (::std::exception)
{
   return s * u.inverse();
}

/**
 * Invert a matrix and scale it by a value
 * @param s the scalar
 * @param u the matrix to be inverted
 @return the scaled inverse of the matrix u
 */
template<class T, class U>
inline ::newton::Matrix< T> operator/(const U& s, const ::newton::Matrix< T>& u) throws(::std::exception)
{
   ::newton::Matrix< T> m = u.inverse();
   return m *= s;
}

template<class T, class U>
inline ::newton::Matrix< T> operator*(const U& s, const ::newton::Matrix< T>& u) throws()
{
   ::newton::Matrix< T> t(u);
   t.scale(s);
   return t;
}

template<class T>
inline bool operator==(const ::newton::Matrix< T>& p, const ::newton::Matrix< T>& q) throws()
{
   return p.equals(q);
}

template<class T>
inline bool operator!=(const ::newton::Matrix< T>& p, const ::newton::Matrix< T>& q) throws()
{
   return !p.equals(q);
}

template<class T>
::std::ostream& operator<<(::std::ostream& out, const ::newton::Matrix< T>& v) throws();

#endif

#ifndef _NEWTON_MATRIX_TPL
#include <newton/Matrix.tpl>
#endif
